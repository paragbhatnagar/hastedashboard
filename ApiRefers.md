# API Endpoints for Merchant Portal

### DASHBOARD ENDPOINTS:

#### 1. Available Stores

Request Type: **POST**
URL: "/dashboardResource/availableStores"
POSTMAN: Dashboard > Available Stores ,

Returns the list of available stores for a particular merchant

POST Object:

```json
{} //Empty body
```
Return Object:
```json
[	// Array of all stores
    {	// Store Object
        value: any // Store syntax in backend 
        text: String // Display name
	},
]
```

#### 2. Dashboard Summary (duration and store constrained)

Request Type: **POST**
URL: "/dashboardResource/dashboardSummary"
POSTMAN: Dashboard > Dashboard Summary ,

Returns Average ticket size, Average basket size, No. of Transactions, Unique Customers by a given time constraint (this week, today, this month or 3 months)

POST Object:

```json
{
    constraint: String // Value of "today" / "thisWeek" / "thisMonth" / "3months"
    store: String[] // Store search tags. If empty, return combined data for all stores
}
```
Return Object:
```json
{
    avgTixSize: Number //The value to be displayed in the dashboard
    avgBasketSize: Number //The value to be displayed in the dashboard
    noTransactions: Number //The value to be displayed in the dashboard
    uniqueCustomers: Number //The value to be displayed in the dashboard
}
```

#### 3. Sales-Time Graph (duration and store constrained)

Request Type: **POST**
URL: "/dashboardResource/sales-TimeGraph"
POSTMAN: Dashboard > Sales Time Graph ,

Returns sales-time graph data points for main graph on dashboard page

POST Object:

```json
{
    constraint: String // Value of "today" / "thisWeek" / "thisMonth" / "3months"
    store: String[] // Store search tags. If empty, return combined data for all stores
}
```
Return Object:

```json
{
    labels: String[] //String Array x-axis, depending on constraint
  	//e.g. If constraint is week then this returns 
    //['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
	datasets:[{
        label:'Sales Volume', // This value will always be "Sales Volume"
        data: Number[], // Number Array for y-axis, depending on constraints
        //The data position here corresponds to the labels above
        //e.g. The 1st number in this array corresponds to the 1st label in labels array
    }]
}
```


#### 4. Promotions Table (duration and store constrained)

Request Type: **POST**
URL: "/dashboardResource/promotionsTable"
POSTMAN: Dashboard > Promotions Table ,

Returns top 10 promotions for Promotions Table on Dashboard Page

POST Object:

```json
{
    constraint: String // Value of "today" / "thisWeek" / "thisMonth" / "3months"
    store: String[] // Store search tags. If empty, return combined data for all stores
}
```
Return Object:
```json
[	// JSON Array sorted by total sales
    {
        rank: Number, //the ranking of the current promotion, determined by total sales
        product: String, //Name of product under this promotion
        quantity: Number, //Quantity of product sold under this promotion
        totalSales: Number //Total sales of this product under this promotion
	},
]

```
#### 5. Categories Table (duration and store constrained)

Request Type: **POST**
URL: "/dashboardResource/categoriesTable"
POSTMAN: Dashboard > Categories Table ,

Returns the top 20 categories for categories table in dashboard

POST Object:

```json
{
    constraint: String // Value of "today" / "thisWeek" / "thisMonth" / "3months"
    store: String[] // Store search tags. If empty, return combined data for all stores
}
```
Return Object:
```json
[	// JSON Array sorted by total sales
    {
        category: String, //Displayed name of the category
    	sales:Number //The total sales for this category
	},
]
```
#### 6. Transactions Table (duration and store constrained)

Request Type: **POST**
URL: "/dashboardResource/transactionsTable"
POSTMAN: Dashboard > Transactions Table ,

Returns the 20 most recent transactions for Transactions table in dashboard

POST Object:

```json
{
    constraint: String // Value of "today" / "thisWeek" / "thisMonth" / "3months"
    store: String[] // Store search tags. If empty, return combined data for all stores
}
```

Return Object:

```json
[	// JSON Array sorted by timestamp
    {
        receiptNo: String, // Transaction Receipt Number
    	timestamp: String, // Transaction Time Formatted is DD/MM/YY hh:mm:ss
        // TODO: Consider using Date Object and Parsing on Frontend
    	noItemsPurchased: Number, // Number of items purchased 
        age: Number, // Customer Age
        gender: Number, // 1 if Male, 0 if Female 
        storeLocation: String, // Location of the store
        totalReceiptAmt: Number // Transaction Amount
	},
]
```


### SALES ENDPOINTS:

#### 1. Available Stores

Request Type: **POST**
URL: "/dashboardResource/availableStores"
POSTMAN: Dashboard > Available Stores ,

Returns the list of available stores for a particular merchant

POST Object:

```json
{} //Empty body
```

Return Object:

```json
[	// Array of all stores
    {	// Store Object
        value: any // Store syntax in backend
        text: String // Display name
	},
]
```

#### 2. Sales Summary (time constrained to week)

Request Type: **POST**
URL: "/salesResource/salesSummary"
POSTMAN: New Sales > Sales Summary (time constrained to week) ,

Returns 

- Total Sales for past week for all Stores in past week, Percentage change in value from 7 days ago
- Total Number of transactions for all Stores in past week, Percent change in value from 7 days ago
- Total Number of Unique Shoppers for all Stores in past week, Percent change in value from 7 days ago

POST Object:

```json
{ // POST Object for sanity check, & in case this becomes modifiable in the future
    constraint: String // Value of "today" / "thisWeek" / "thisMonth" / "3months"
    // In this case, it will always be "thisWeek"
    store: String[] // Store search tags. If empty, return combined data for all stores
    // In this case, it will always be empty
}
```

Return Object:

```json
{
    totalSales: {
        value: Number, //Number of sales in week for all stores
        percentage: Number // Percentage increase/decrease
    },
    noTransactions: {
        value: Number, //Number of transactions in week for all stores
        percentage: Number // Percentage increase/decrease
    },
    uniqueCustomers: {
        value: Number, //Number of unique customers in week for all stores
        percentage: Number // Percentage increase/decrease
    },
}
```

#### 3. Product-Category Breakdown

Request Type: **POST**
URL: "/salesResource/productCategoryBreakdown"
POSTMAN: New Sales > Product-Category Breakdown ,
POST Object:

```json
{
    store: String[] // Store search tags - in case of different products in stores. 
    // If empty, return combined data for all stores
}
```

Return Object:

```json
{
    categories: [ // Array of categories
 		name: String, // Display name of Category
        value: String // Internal category label
        products: [ // Array of products for each category
        	name: String, // Display name of Product
        	value: String // Internal product label
    	]
 	]
}
```

#### 4. Analysis Sales-Time Graph (multiple factors constrained)

Request Type: **POST**
URL: "/salesResource/analysisSales-TimeGraph"
POSTMAN: New Sales > 
Returns sales-time graph data points for overall sales graph on sales page

POST Object:

```json
{
    duration: String // Value of "day" / "week" / "month" / "quarter" / "year"
    ageStart: Number // Start of age Range - Easier sanity check than DOB Start
    ageEnd: Number // End of age Range - Easier sanity check than DOB Start
    category: String // Key of Category. May be null
    product: String // Key of Product. May be null. If Category is null, will be null
    store: String[] // Store search tags. If empty, return combined data for all stores
}
```

Return Object:

```json
{
    labels: String[] //String Array x-axis, depending on constraint
  	//e.g. If constraint is week then this returns 
    //['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
	datasets:[{
        label: 'Sales Volume', // This value will always be "Sales Volume"
        data: Number[], // Number Array for y-axis, depending on constraints
        //The data position here corresponds to the labels above
        //e.g. The 1st number in this array corresponds to the 1st label in labels array
    }]
}
```

#### 5. Analysis Average Basket Graph (multiple factors constrained)

Request Type: **POST**
URL: "/salesResource/analysisAverage-TicketGraph"

Returns average basket size graph data points for basket graph on sales page

POST Object:

```json
{
    duration: String // Value of "day" / "week" / "month" / "quarter" / "year"
    ageStart: Number // Start of age Range - Easier sanity check than DOB Start
    ageEnd: Number // End of age Range - Easier sanity check than DOB Start
    category: String // Key of Category. May be null
    product: String // Key of Product. May be null. If Category is null, will be null
    store: String[] // Store search tags. If empty, return combined data for all stores
}
```

Return Object:

```json
{
    labels: String[] //String Array x-axis, depending on constraint
  	//e.g. If constraint is week then this returns 
    //['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
	datasets:[{
        label: 'Average Basket', // This value will always be "Average Basket"
        data: Number[], // Number Array for y-axis, depending on constraints
        //The data position here corresponds to the labels above
        //e.g. The 1st number in this array corresponds to the 1st label in labels array
    }]
}
```

#### 6. Analysis Average Ticket Graph (multiple factors constrained)

Request Type: **POST**
URL: "/salesResource/analysisAverage-TimeGraph"

Returns average ticket size graph data points for ticket graph on sales page

POST Object:

```json
{
    duration: String // Value of "day" / "week" / "month" / "quarter" / "year"
    ageStart: Number // Start of age Range - Easier sanity check than DOB Start
    ageEnd: Number // End of age Range - Easier sanity check than DOB Start
    category: String // Key of Category. May be null
    product: String // Key of Product. May be null. If Category is null, will be null
    store: String[] // Store search tags. If empty, return combined data for all stores
}
```

Return Object:

```json
{
    labels: String[] //String Array x-axis, depending on constraint
  	//e.g. If constraint is week then this returns 
    //['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
	datasets:[{
        label: 'Average Ticket', // This value will always be "Average Ticket"
        data: Number[], // Number Array for y-axis, depending on constraints
        //The data position here corresponds to the labels above
        //e.g. The 1st number in this array corresponds to the 1st label in labels array
    }]
}
```
### SHOPPERS ENDPOINTS:


Request Type: **POST**
URL: "/shoppersResource/demographicsSummary

Returns average ticket size graph data points for ticket graph on sales page

POST Object:

```json
{
    store: String[], // Store search tags. If empty, return combined data for all stores
    startDate: moment(),
    endDate: moment(),
    age:string, // <20,20s,30s,40s,50s,>60s 
}
```

Return Object:

```json
{
    avgBasketSize:,
    avgTicketSize:,
    topCategory:,
    topPromo:,
    frequentVisit: //Monday, 11pm-2pm for 12 mins
}
```

Request Type: **POST**
URL: "/shoppersResource/shopperTypes

Returns average ticket size graph data points for ticket graph on sales page

POST Object:

```json
{
    store: String[] // Store search tags. If empty, return combined data for all stores
}
```

Return Object:

```json
{
[   {profile: 'A', desc:'Healthy and natural', age:'21', gender:'F', avgbasket:5, ticketsize:2},
    {profile: 'B', desc:'Gourmet focus', age:'25', gender:'M', avgbasket:5, ticketsize:2},
    {profile: 'C', desc:'Family feeders ', age:'30', gender:'M', avgbasket:5, ticketsize:2},
    {profile: 'D', desc:'On-the-go', age:'40', gender:'F', avgbasket:5, ticketsize:2},
    {profile: 'E', desc:'Variety seekers', age:'60', gender:'M', avgbasket:5, ticketsize:2},
    {profile: 'F', desc:'Here we go again', age:'80', gender:'M', avgbasket:5, ticketsize:2}
        ]            
}
```


Request Type: **POST**
URL: "/shoppersResource/profileVisitFrequency

Returns average ticket size graph data points for ticket graph on sales page

POST Object:

```json
{
   store: String[], // Store search tags. If empty, return combined data for all stores
   startDate: moment(),
   endDate: moment(),
   profileName : String // A,B,C Based on above
   }
```

Return Object:

```json

{
    data: {  //This is default data
        labels: ['Apr 15', 'Apr 16', 'Apr 17', 'Apr 18', 'Apr 19', 'Apr 20', 'Apr 21'],
        datasets: [
            {
                label: 'Visit Frequency ',
                data: [2, 4, 5, 0, 7, 6, 4],
            }
        ]
    }
    
}
```

Request Type: **POST**
URL: "/shoppersResource/profileSummary

Returns average ticket size graph data points for ticket graph on sales page

POST Object:

```json
{
   store: String[], // Store search tags. If empty, return combined data for all stores
   startDate: moment(),
   endDate: moment(),
   profileName : String // A,B,C Based on above
}
```

Return Object:

```json
{
  avgProfitMargin:,
  avgVisits:,
  priceSensitivity:

}
```

Request Type: **POST**
URL: "/shoppersResource/profileRevenue

Returns average ticket size graph data points for ticket graph on sales page

POST Object:

```json
{
   store: String[], // Store search tags. If empty, return combined data for all stores
     startDate: moment(),
     endDate: moment(),
     profileName : String // A,B,C Based on above
}
```

Return Object: // TREEMAP

```json
{
    dataTreeMap: {
                    "name": "ALL CATEGORIES",
                    "children": [
                            {   "name": "FOOD",
                            "children":[
    
                                {"name": "FRESH PRODUCE",
                                "children": [
                                    {
                                        "name": "FRUITS",
                                        "children": [
                                            {
                                                "name": "Apples",
                                                "value": 100,
                                            },
                                            {"name": "Pears", "value": 80},
                                            {"name": "Strawberries", "value": 80},
                                            {"name": "Peaches", "value": 20}
                                        ]
                                    },
                                    {
                                        "name": "MEAT",
                                        "children": [
                                            {
                                                "name": "MEAT",
                                                "children": [
                                                    {"name": "Pork", "value": 80},
                                                    {"name": "Chicken", "value": 100},
                                                    {"name": "Duck", "value": 50},
                                                    {"name": "Beef", "value": 200},
                                                    {"name": "Seafood", "value": 300}
    
                                                ]
                                    },
                                    {
                                        "name": "DAIRY",
                                        "children": [
                                            {"name": "Chocolate Milk", "value": 80},
                                            {"name": "Skimmed Milk", "value": 110},
                                            {"name": "Full Cream Milk", "value": 110},
                                            {"name": "Low Fat Milk", "value": 110}
                                        ]
                                    },
                                    {
                                        "name": "BREAD, CEREAL",
                                        "children": [
                                            {"name": "KoKo Crunch", "value": 90},
                                            {"name": "Muesli", "value": 50},
                                            {"name": "Gardenia", "value": 90},
                                            {"name": "Milk Bread", "value": 20},
                                            {"name": "Honey Stars", "value": 60}
    
    
                                        ]
                                    }
                                ]
                                }]}]},
    
                        {
                            "name": "NON-FOOD",
                            "children": [
                                {"name": "Others", "value": 150},
                                {"name": "FunctionSequence", "value": 10},
                                {
                                    "name": "HOUSEHOLD",
                                    "children": [
                                        {"name": "Tote", "value": 100},
                                        {"name": "Laundry Basket", "value": 20},
                                        {"name": "Tupperware", "value": 30},
                                        {"name": "Pans", "value": 40},
                                        {"name": "Baking Needs", "value": 10},
                                        {"name": "Storage Bin", "value": 10},
                                    ]
                                },{
                                    "name": "BLADES",
                                    "children": [
                                        {"name": "Sharp", "value": 20},
                                        {"name": "Swift", "value": 30},
                                        {"name": "SuperBlade", "value": 50},
                                        {"name": "Gatorade", "value": 10},
                                    ]
                                },{
                                    "name": "DIAPERS",
                                    "children": [
                                        {"name": "Comfy", "value": 100},
                                        {"name": "Uber", "value": 100},
    
                                    ]
                                }
                            ]
                        },
                        {
                            "name": "DRINKS",
                            "children": [
                                {
                                    "name": "Alcohol",
                                    "children": [
                                        {"name": "Beer", "value": 50},
                                        {"name": "Wine", "value": 55},
                                        {"name": "Hard Liquor", "value": 120},
                                        {"name": "Spirits", "value": 60},
                                        {"name": "Cocktails", "value": 90}
                                    ]
                                }
                            ]
                        }
    
                            ]}
            }
        }
}
```


Request Type: **POST**
URL: "/shoppersResource/profileProfit

Returns average ticket size graph data points for ticket graph on sales page

POST Object:

```json
{store: String[], // Store search tags. If empty, return combined data for all stores
      startDate: moment(),
      endDate: moment(),
      profileName : String // A,B,C Based on above
 }
```

Return Object:

```json
{
   data:[
       { label: 'MEAT', value: 8 },
       { label: 'FRUITS', value: 6 },
       { label: 'ALCOHOL', value: 20 },
       { label: 'HOUSEHOLD', value: 6 },
       { label: 'PARTY NEEDS', value: 6 },
       { label: 'BEAUTY', value: 6 },
       { label: 'SNACKS', value: 9 }
                                   ]
}
```




