import React, { Component } from 'react';
import { Icon, Button, Dimmer, Loader} from 'semantic-ui-react'

//import components 
import SidebarMenu from '../components/sidebar';
import Navbar from '../components/navbar';
import Analysis from './components/analysis';
import Reports from './components/reports';

// import styling and assets
import '../assets/css/sales.css';
import '../assets/css/App.css';
import Transactions from '../dashboard/components/dashboardTransactions'

// import api
import {
    API_SERVER_URL, 
    PAST_WEEK_SALES_SUMMARY,
} from '../api';

class Sales extends Component {
    constructor(props){
        super(props);
        this.state = {
            is_loading: false,
            merchant_name: localStorage.getItem("merchant"),
            session: localStorage.getItem("session"),

            salesDataSummary: {
                total_sales: {
                    value: 0,
                    percentage: null
                },
                no_of_transactions: {
                    value: 0,
                    percentage: null
                },
                unique_customers: {
                    value: 0,
                    percentage: null
                },
            },
            tabSelected: 'analysis',
            durationOptions:[],
            storeOptions: [],
        }
    }

    async componentDidMount(){
        this.setState({is_loading:true});
        console.log("session: ", this.state.session)
        console.log("merchant: ", this.state.merchant_name)
        if(this.state.session == null || this.state.merchant_name == null) {
            // return <Redirect to=''/>
            window.location.href = '/';
        }
        
       await this.loadSalesSummary();
       this.setState({is_loading:false});
    }
    
    async loadSalesSummary() {
        const body = {
            "merchant_name": this.state.merchant_name
        }
        let salesSummaryResult = await this.callPostAPI(PAST_WEEK_SALES_SUMMARY, body);
        if(!salesSummaryResult.error){
            console.log("sales summary result");
            console.log(salesSummaryResult);
            this.setState({
                salesDataSummary: {
                    total_sales: {
                        value: salesSummaryResult.total_sales.this_week_sales,
                        percentage: salesSummaryResult.total_sales.sales_percentage_change
                    },
                    no_of_transactions: {
                        value: salesSummaryResult.no_of_transactions.this_week_no_of_transactions,
                        percentage: salesSummaryResult.no_of_transactions.transactions_percentage_change
                    },
                    unique_customers: {
                        value: salesSummaryResult.unique_customers.this_week_unique_customers,
                        percentage: salesSummaryResult.unique_customers.unique_customers_percentage_change
                    },
                },
            })
        }
        console.log(this.state.salesDataSummary)
    }

    async callGetAPI(api){
        let result = {};
        await fetch(API_SERVER_URL + api,{
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
        .then((response) => { 
            console.log("GET response: ", response)
            response.json()
        })
        .then((responseJson)=>{
            result = responseJson;
        })

        return result;
    }

    async callPostAPI(api, body){
        let result = {};
        
        await fetch(API_SERVER_URL + api,{
            method: 'POST', 
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
        .then((response) => response.json())
        .then((responseJson)=>{
            result = responseJson;
        })
        return result;
    }

    render() {
        return (
            <div style={{display: 'flex'}}>
            <Dimmer active={this.state.is_loading}>
                <Loader> Cool things are coming your way... </Loader>
            </Dimmer>
                <SidebarMenu page="sales"/>
                <div className="sales-container">
                    <Navbar 
                        refresh={this.refreshData}
                    />
                    <div className="sales-stats">
                        <div className="card" id="avg-ticket-size">
                            <h3 className="title">Total Sales</h3>
                            <h2 className="value">
                                ${this.state.salesDataSummary.total_sales.value > 0 ?
                                    this.state.salesDataSummary.total_sales.value.toLocaleString(navigator.language, { minimumFractionDigits: 2 })
                                    :
                                    "$0.00"
                                }
                            </h2>
                            { this.state.salesDataSummary.total_sales.percentage !== null ? (
                                <div className="ticker">
                                    <Icon size="large" name={this.state.salesDataSummary.total_sales.percentage>0 ? "caret up" : "caret down"}
                                        className={"icon " + (this.state.salesDataSummary.total_sales.percentage>0 ? "up" : "down")}
                                    />
                                    <p>{this.state.salesDataSummary.total_sales.percentage}% from last week</p>
                                </div>
                            ) : (
                                <div className="ticker">
                                </div>
                            )}
                            
                        </div>
                        
                        <div className="card" id="no-of-transactions">
                            <h3 className="title">No. of Transactions</h3>
                            <h2 className="value">
                                {console.log('sales data summary')}
                                {console.log(this.state.salesDataSummary.value)}
                                {this.state.salesDataSummary.no_of_transactions.value > 0 ?
                                    this.state.salesDataSummary.no_of_transactions.value.toLocaleString(navigator.language, { minimumFractionDigits: 0 })
                                    :
                                    0
                                }
                            </h2>
                            { this.state.salesDataSummary.no_of_transactions.percentage !== null ? (
                                <div className="ticker">
                                    <Icon size="large"
                                        name={this.state.salesDataSummary.no_of_transactions.percentage>0 ? "caret up" : "caret down"} 
                                        className={"icon " + (this.state.salesDataSummary.no_of_transactions.percentage>0 ? "up" : "down")}
                                    />
                                    <p>{this.state.salesDataSummary.no_of_transactions.percentage}% from last week</p>
                                </div>
                            ) : (
                                <div className="ticker">
                                </div>
                            )}
                        </div>

                        <div className="card" id="avg-basket-size">
                            <h3 className="title">Unique Customers</h3>
                            <h2 className="value">
                                {this.state.salesDataSummary.unique_customers.value > 0 ?
                                    this.state.salesDataSummary.unique_customers.value.toLocaleString(navigator.language, { minimumFractionDigits: 0 })
                                    :
                                    0
                                }
                            </h2>
                            { this.state.salesDataSummary.unique_customers.percentage !== null ? (
                                <div className="ticker">
                                    <Icon size="large" name={this.state.salesDataSummary.unique_customers.percentage>0 ? "caret up" : "caret down"} 
                                        className={"icon " + (this.state.salesDataSummary.unique_customers.percentage>0 ? "up" : "down")}
                                    />
                                    <p>{this.state.salesDataSummary.unique_customers.percentage}% from last week</p>
                                </div>
                            ) : (
                                <div className="ticker">
                                </div>
                            )}
                           
                        </div>
                    </div>
                    <div className="card">
                        <div className="sales-buttons">
                            <Button 
                                fluid
                                className={ "button " + (this.state.tabSelected === "analysis" ? "active" : "" )}
                                onClick ={() => 
                                    this.setState({
                                        tabSelected: "analysis"
                                    })
                                }>
                                Analysis
                            </Button>
                            <Button 
                                fluid
                                className={ "button " + (this.state.tabSelected === "reports" ? "active" : "" )}
                                onClick={()=> 
                                    this.setState({
                                        tabSelected: "reports"
                                    })
                                }>
                                Reports
                            </Button>
                        </div>
                        {this.state.tabSelected === "analysis"? (
                            <Analysis/>
                        ) : (
                            <Reports />
                        )}
                    </div>
                    
                </div>
            </div> 
        );
    }
}

export default Sales;
