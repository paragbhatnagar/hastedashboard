import React, { Component } from 'react';
import { Button, Dropdown } from 'semantic-ui-react'
//import components

// import styling and assets
import '../../assets/css/sales.css';
import BreakdownReport from "./breakdownReport";

//!!RESOURCE DESTINATION NEEDS TO BE UPDATED.
import {API_SERVER_URL, AVAILABLE_STORES, REPORT_ALL} from "../../api";
// import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/en-gb';
import Yearly from "./reportForms/yearly";
import Weekly from "./reportForms/weekly";
import Quarterly from "./reportForms/quarterly";
import Hourly from "./reportForms/hourly";
import Daily from "./reportForms/daily";
import Monthly from "./reportForms/monthly";



class Reports extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reportDisplay: "Financials",
            periodOptions: [
                {value: 'hour', text: 'Hourly'},
                {value: 'day', text: 'Daily'},
                {value: 'week', text: 'Weekly'},
                {value: 'month', text: 'Monthly'},
                {value: 'quarter', text: 'Quarterly'},
                {value: 'year', text: 'Yearly'}
            ],
            monthOptions: [
                { value:  '01', text: 'Jan' },
                { value:  '02', text: 'Feb' },
                { value:  '03', text: 'Mar' },
                { value:  '04', text: 'Apr' },
                { value:  '05', text: 'May' },
                { value:  '06', text: 'Jun' },
                { value:  '07', text: 'Jul' },
                { value:  '08', text: 'Aug' },
                { value:  '09', text: 'Sep' },
                { value: '10', text: 'Oct' },
                { value: '11', text: 'Nov' },
                { value: '12', text: 'Dec' },
            ],

            quarterOptions: [
                { value: 1, text: 'Q1' },
                { value: 2, text: 'Q2' },
                { value: 3, text: 'Q3' },
                { value: 4, text: 'Q4' },
            ],

            downloadOutputs:undefined,
            displayDownloads:false,

            generatedData: {
                title:null,
                headers:[],
                body:[]
            },

            selectedStores: undefined,
            storeOptions:undefined,

            period: 'week',

            //For hourly
            hourlyDate: undefined,

            //For daily
            startDateDaily: undefined,
            endDateDaily: undefined,

            //For weekly
            startDateWeek: undefined,
            endDateWeek: undefined,

            //For year
            startYear: undefined,
            endYear: undefined,
            
            //For month
            startUnitMonth:undefined,
            endUnitMonth:undefined,

            //For quarter
            quarterStart:undefined,
            quarterEnd:undefined,
        }
    }

    async componentDidMount(){
        
        const today = moment()
        //Get current month
        const thisMonthNum = today.month()+1;
        let thisMonthStr = ""+thisMonthNum;

        if(thisMonthNum<10){
            thisMonthStr = "0"+thisMonthStr
        }

        this.setState({
            //For hourly
            hourlyDate: today,

            //For daily
            startDateDaily: today,
            endDateDaily: today,

            //For weekly
            startDateWeek: today,
            endDateWeek: today,

            //For year
            startYear:today.year(),
            endYear:today.year(),

            //For month
            startUnitMonth: thisMonthStr,
            endUnitMonth: thisMonthStr,

            //For quarter
            quarterStart: 1,
            quarterEnd: 1
        });

      //  this.getStores();
    }

    //Universal Calls
    //Parse in the api url and body, used for whole document
    async callPostAPI(api, body){
        let result = {};
        console.log(""+API_SERVER_URL + api);
        await fetch(API_SERVER_URL + api,{

            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
            .then((response) => response.json())
            .then((responseJson)=>{
                result = responseJson;
            })

        return result;
    }

    //INTEGRATION
    async getStores(){
        let s = await this.callPostAPI(AVAILABLE_STORES)
        let stores = s.map((store) => {
            return {
                value: store.outlet_id,
                text: store.outlet_name,
            }
        });
        this.setState({
            storeOptions: stores
        });
    }
    //Generate Reports

    async generateReports () {
        let selectedStores = await this.state.selectedStores;
        if(selectedStores === undefined ||selectedStores.length<1){
            selectedStores = [];
            this.state.storeOptions.map((curstore) => {
                selectedStores.push(curstore.value);
                return null;
            });
        }
       // const stores_json = JSON.parse(selectedStores);//stores_json
        const outlets = selectedStores.map((curStore) => {
            let outletString = "{\"outlet_id\":"+curStore+"}";
            let curStoreString = outletString;
            //Return in JSON format
            return JSON.parse(curStoreString);
        });

        let body= undefined;
        switch (this.state.period) {
            case "hour" :
                body = {
                    report_type: this.state.reportDisplay,
                    group_option: this.state.period,
                    start_date: this.state.hourlyDate,
                    end_date: null,
                    start_unit_month: null,
                    end_unit_month: null,
                    start_year: null,
                    end_year: null,
                    outlets: outlets
                }
                break;
            case "day" :
                body = {
                    report_type: this.state.reportDisplay,
                    group_option: this.state.period,
                    start_date: this.state.startDateDaily,
                    end_date: this.state.endDateDaily,
                    start_unit_month: null,
                    end_unit_month: null,
                    start_year: null,
                    end_year: null,
                    outlets: outlets
                }
                break;
            case "week" :
                body = {
                    report_type: this.state.reportDisplay,
                    group_option: this.state.period,
                    start_date: this.state.startDateWeek,
                    end_date: this.state.endDateWeek,
                    start_unit_month: null,
                    end_unit_month: null,
                    start_year: null,
                    end_year: null,
                    outlets: outlets
                }
                break;
            case "year" :
                body = {
                    report_type: this.state.reportDisplay,
                    group_option: this.state.period,
                    start_date: "2018-01-01",
                    end_date: "2018-01-01",
                    start_unit_month: "01",
                    end_unit_month: "02",
                    start_year: this.state.startYear,
                    end_year: this.state.endYear,
                    outlets: outlets
                }
                break;
            case "month" :
                body = {
                    report_type: this.state.reportDisplay,
                    group_option: this.state.period,
                    start_date: null,
                    end_date: null,
                    start_unit_month: this.state.startUnitMonth,
                    end_unit_month: this.state.endUnitMonth,
                    start_year: this.state.startYear,
                    end_year: this.state.endYear,
                    outlets: outlets
                }
                break;
            case "quarter" :
                body = {
                    report_type: this.state.reportDisplay,
                    group_option: this.state.period,
                    start_date: null,
                    end_date: null,
                    start_unit_month: this.state.startUnitMonth,
                    end_unit_month: this.state.endUnitMonth,
                    start_year: this.state.quarterStart,
                    end_year: this.state.quarterEnd,
                    outlets: outlets
                }
                break;
            default:
                break;
        }

        const rawResults = await this.callPostAPI(REPORT_ALL,body)
        if(!rawResults.error){
            this.setState({
                downloadOutputs:rawResults,
                displayDownloads:true
            })
        }
    }



    //**************Handlers**************//
    setPeriodOptions(display){
        if(display==="financials"){
            this.setState({
                periodOptions: [
                    {value: 'hour', text: 'Hourly'},
                    {value: 'day', text: 'Daily'},
                    {value: 'month', text: 'Monthly'},
                    {value: 'quarter', text: 'Quarterly'},
                    {value: 'year', text: 'Yearly'}
                ]
            })
        }else if(display==="analytics"){
            this.setState({
                periodOptions: [
                    {value: 'week', text: 'Weekly'},
                    {value: 'month', text: 'Monthly'},
                    {value: 'quarter', text: 'Quarterly'},
                    {value: 'year', text: 'Yearly'}
                ]
            })
        }else{
            this.setState({
                periodOptions: [

                    {value: 'day', text: 'Daily'},
                    {value: 'week', text: 'Weekly'},
                    {value: 'month', text: 'Monthly'}
                ]
            })
        }

    }
    /************Methods***************/

    //Handle Hourly Date Change
    async handleHourlyDateChange(date) {
        this.setState({
            hourlyDate:date,
        })
    }
    /************************************ */
    //Handle Daily Date Change
    async handleDailyStartDateChange(date) {
        if (date < this.state.endDateDaily) {
            await this.setState({
                startDateDaily: date
            });
        }
    }

    async handleDailyEndDateChange(date) {
        if (date > this.state.startDateDaily) {
            await this.setState({
                endDateDaily: date
            });
        }
    }

    /************************************ */
    //Handle Weekly Date Change
    async handleWeeklyStartDateChange(date) {
        if (date < this.state.endDateWeek) {
            await this.setState({
                startDateWeek: date
            });
        }
    }

    async handleWeeklyEndDateChange(date) {
        if (date > this.state.startDateWeek) {
            await this.setState({
                endDateWeek: date
            });
        }
    }
    /************************************ */
    //Handle Month Change
    async handleStartMonthChange(month) {
        const monthNum = parseInt(month);
        const endMonthNum = parseInt(this.state.endUnitMonth);
        if(monthNum>endMonthNum){
            this.setState({
                startUnitMonth:month
            })
        }
    }

    async handleEndMonthChange(month) {
        console.log(month)
        if(!(this.state.endYear===this.state.startYear && this.state.startUnitMonth>month)){
            await this.setState({
                endUnitMonth:month.value
            })
        }
    }
    /************************************ */
    //Handle Quarter Change
    async handleStartQuarterChange(quarter) {
        this.setState({
            quarterStart:quarter
        })
    }

    async handleEndQuarterChange(quarter) {
        this.setState({
            quarterEnd:quarter
        })
    }

    /************************************ */
    //Handle Year
    async handleStartYearChange(year) {
        if(year<=this.state.endYear){
            this.setState({
                startYear:year
            })
        }
    }

    async handleEndYearChange(year) {
        if(year>=this.state.startYear){
            this.setState({
                endYear:year
            })
        }
    }
    /************************************ */

    render() {
        return (
            <div>
                <div style={{background:'silver',alignSelf:'center',textAlign:'center',height:'40px'}}><h2 className="title" style={{color:'black',alignSelf:'center'}}>Work in Progress</h2></div>
                <div className="sales-buttons">
                    <Button
                        fluid
                        className={"button " + (this.state.reportDisplay === "Financials" ? "active" : "")}
                        onClick={() => {this.setState({reportDisplay: "Financials"})
                                        this.setPeriodOptions("financials")}}>
                        Financials
                    </Button>

                    <Button
                        fluid
                        className={"button " + (this.state.reportDisplay === "Inventory" ? "active" : "")}
                        onClick={() => {this.setState({reportDisplay: "Inventory"})
                                        this.setPeriodOptions("inventory")}}>
                        Inventory
                    </Button>

                    <Button
                        fluid
                        className={"button " + (this.state.reportDisplay === "Analytics" ? "active" : "")}
                        onClick={() => {this.setState({reportDisplay: "Analytics"})
                                        this.setPeriodOptions("analytics")}}>
                        Analytics
                    </Button>
                </div>
                <div className="analysis" style={{display:"flex"}}>

                <div className="analysis-options" style={{alignSelf:"flex-start", paddingTop:'10px'}}>
                    <div className="input-container full">
                        <p className="label">Store</p>
                        <Dropdown className="multiple-dropdown-selector" placeholder='Search for stores'
                                  options={this.state.storeOptions} style={{flex:1}}
                                  multiple search selection
                                  onChange={async (event,data) => {
                                      await this.setState({ selectedStores: data.value })
                                      console.log("Store value: ", this.state.selectedStores)
                                  }}
                        />
                    </div>
                </div>

                <div style={{display:'flex'}}>
                {/*get period*/}
                <div className="input-container" style={{flex:1, paddingLeft: 20}}>
                    <p className="label">Frequency</p>
                    <Dropdown className="dropdown-selector"
                            defaultValue={this.state.period}
                            selection options={this.state.periodOptions}
                            onChange={async (event,data)=>{await this.setState({ period : data.value})}}

                    />
                </div>

                {/*1. HOUR State */}
                { this.state.period === 'hour' ? (
                    <Hourly hourlyDate = {this.state.hourlyDate}
                            handleHourlyDateChange={this.handleHourlyDateChange.bind(this)}
                    ></Hourly>
                ) : null}

                {/*2. DAY State */}
                { (this.state.period === 'day') ? (
                    <Daily handleDailyStartDateChange={this.handleDailyStartDateChange.bind(this)}
                           handleDailyEndDateChange={this.handleDailyEndDateChange.bind(this)}
                           startDateDaily = {this.state.startDateDaily}
                           endDateDaily = {this.state.endDateDaily} ></Daily>
                ) : null}

                {/* 3. WEEKstart State */}

                { this.state.period === 'week' ? (
                    <Weekly handleWeeklyStartDateChange={this.handleWeeklyStartDateChange.bind(this)}
                            handleWeeklyEndDateChange={this.handleWeeklyEndDateChange.bind(this)}
                            startDateWeek = {this.state.startDateWeek}
                            endDateWeek = {this.state.endDateWeek} ></Weekly>
                ) : null}

                {/*4. MonthStart State */}
                { this.state.period === 'month' ? (
                   <Monthly 
                            //Year side
                            handleStartYearChange={this.handleStartYearChange.bind(this)}
                            handleEndYearChange={this.handleEndYearChange.bind(this)}
                            startYear = {this.state.startYear}
                            endYear = {this.state.endYear}
                            //Month Side
                            monthOptions = {this.state.monthOptions}
                            startUnitMonth = {this.state.startUnitMonth}
                            endUnitMonth = {this.state.endUnitMonth}
                            handleStartMonthChange= {this.handleStartMonthChange.bind(this)}
                            handleEndMonthChange= {this.handleEndMonthChange.bind(this)}></Monthly>
                ) : null}

                {/* Quarterly State */}
                { this.state.period === 'quarter' ? (
                   <Quarterly //For Year
                              startYear ={this.state.startYear}
                              endYear ={this.state.endYear}
                              handleEndYearChange = {this.handleEndYearChange.bind(this)}
                              handleStartYearChange = {this.handleStartYearChange.bind(this)}
                              //For Quarter
                              startQuarter = {this.state.quarterStart}
                              endQuarter = {this.state.quarterEnd}
                              quarterOptions ={this.state.quarterOptions}
                              handleStartQuarterChange={this.handleStartQuarterChange.bind(this)}
                              handleEndQuarterChange={this.handleEndQuarterChange.bind(this)}
                   ></Quarterly>
                ) : null}

                {/* Yearly State */}
                { this.state.period === 'year' ? (
                    <Yearly handleStartYearChange={this.handleStartYearChange.bind(this)}
                            handleEndYearChange={this.handleEndYearChange.bind(this)}
                            startYear = {this.state.startYear}
                            endYear = {this.state.endYear}></Yearly>
                ) : null}

                    <div className="input-container" style={{flex:1, paddingRight: 20}}>
                        <Button style={{flex:1, paddingRight:30}}
                            className="button"
                            id="data-button"
                            onClick={this.generateReports.bind(this)}>
                            Generate Reports
                        </Button>
                    </div>

                </div>
                </div>

                <div >
                    <div style={(this.state.reportDisplay === "Financials" ? null : {display: 'none'})}>
                        {!this.state.displayDownloads ?(null):(
                            <div>
                            <div className="cards-container">
                                <BreakdownReport title={"Profit and Loss Report"} headers={"Financials"}
                                                generatedData={ this.state.downloadOutputs.PROFIT_AND_LOST_REPORT}/>
                                <BreakdownReport title={"Transaction Report"} headers={"Financials"}
                                                generatedData={ this.state.downloadOutputs.TRANSACTION_REPORT}/>
                                <BreakdownReport title={"Daily Sales Report"} headers={"Financials"}
                                                generatedData={ this.state.downloadOutputs.DAILY_SALES_REPORT}/>
                            </div>
                            <div className="cards-container">
                                <BreakdownReport title={"Hourly Sales Report"} headers={"Financials"}
                                                generatedData={ this.state.downloadOutputs.HOURLY_SALES_REPORT}/>
                                <BreakdownReport title={"Item Sales Report"}  headers={"Financials"}
                                                generatedData={ this.state.downloadOutputs.ITEM_SALES_REPORT}/>
                                <BreakdownReport title={"Member Sales Report"} headers={"Financials"}
                                                generatedData={ this.state.downloadOutputs.MEMBER_SALES_REPORT}/>
                            </div>
                            </div>
                        )}
                    </div>
                    <div style={(this.state.reportDisplay === "Inventory" ? null : {display: 'none'})}>
                        {!this.state.displayDownloads?(null)
                            :(
                            <BreakdownReport title={"Inventory Report"} headers={"Inventory"}
                                            generatedData={ this.state.downloadOutputs.INVENTORY_REPORT}/>
                        )}

                    </div>
                    <div style={(this.state.reportDisplay === "Analytics" ? null : {display: 'none'})}>
                        {!this.state.displayDownloads?(null):(
                            <div>
                                <BreakdownReport title={"Fastest and Slowest Goods Report"} headers={"Analytics"}
                                                generatedData={ this.state.downloadOutputs.FASTEST_AND_SLOWEST_GOODS_REPORT}/>
                                <BreakdownReport title={"Periodic Comparison Report"} headers={"Analytics"}
                                                generatedData={ this.state.downloadOutputs.PERIODIC_COMPARISON_REPORT}/>
                            </div>
                        )}

                    </div>
                </div>
        </div>
        ); }
}
export default Reports;