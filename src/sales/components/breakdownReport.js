import React, { Component } from 'react';
import Icon from "semantic-ui-react/dist/es/elements/Icon/Icon";
import Card from "semantic-ui-react/dist/es/views/Card/Card";
import moment from "moment/moment";
import {map} from "moment/src/lib/utils/map";
import {CSVLink} from 'react-csv';
import Button from "semantic-ui-react/dist/es/elements/Button/Button";

// import styling and assets
import '../../assets/styles/sales.scss';

class BreakdownReport extends Component {

    constructor(props) {
        super(props);
    }

    // ##############################
    // // // Renderers
    // ##############################
    render() {

        return (
            <div className="report">
            <div className="report-cards" style={{margin: '20px',flex:1}}>

                <Card className="card" >
                        <div className="text">
                            <h3 className="title">{this.props.headers}</h3>
                            <h4 className="value">{this.props.title}</h4>
                        </div>
                        {(this.props.generatedData!=undefined)? (
                        <CSVLink
                            filename={this.props.generatedData.title}
                            data={this.props.generatedData.body}
                            headers={this.props.generatedData.headers}>
                            <Button className="button">
                                <Icon size="big" name="download"/>
                            </Button>
                        </CSVLink>
                            ):(
                                <Button className="button" S >
                                    <Icon size="big" name="download" />
                                </Button>
                            )}
                </Card>
            </div>
            </div>
        )

    }
}
export default BreakdownReport;