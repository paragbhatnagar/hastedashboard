import React, { Component } from 'react';
import { Dropdown, Input, Button , Divider} from 'semantic-ui-react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/en-gb';
import 'react-select/dist/react-select.css';
import 'react-virtualized/styles.css';
import 'react-virtualized-select/styles.css';
import Select from 'react-virtualized-select';
import spinner from '../../assets/img/spinner.gif';

import {
    API_SERVER_URL, 
    AVAILABLE_STORES,
    CATEGORIES,
    // SUBCATEGORIES,
    PRODUCTS,
    SALES_ANALYSIS,
} from '../../api';

//import components 
import Chart from '../../components/chart';

// import styling and assets
import '../../assets/styles/sales.scss';
//import {parse} from 'json2csv'

class Analysis extends Component {
    constructor(props){
        super(props);
        this.state = {
            body: {
                merchant_name: localStorage.getItem("merchant"),
            },
            session: localStorage.getItem("session"),

            daysInEachMth: [0, 31, 28, 31, 30, 31, 30,
                        31, 31, 30, 31, 30, 31],
            
            quarterPeriod: ["", "1", "1", "1",
                            "2", "2", "2", 
                            "3", "3", "3", 
                            "4", "4", "4"],

            startOfMthInEachQuarter: ["0", "01", "04", "07", "10"],

            storeOptions: [],
            selectedStores: [],

            categoryOptions: [],
            selectedCategories: [], 
            isAllCatSelected: true,

            productOptions: [], 
            selectedProducts: [],
            formatSelectedProducts: [],

            durationOptions: [
                { value: 'Daily', text: 'Daily' }, 
                { value: 'Weekly', text: 'Weekly' },
                { value: 'Monthly', text: 'Monthly' },
                { value: 'Quarterly', text: 'Quarterly'},
                { value: 'Yearly', text: 'Yearly' }
            ],
            selectedDuration: "Daily",

            yearOptions: [
                { value: '2017', text: '2017' },
                { value: '2018', text: '2018' },
            ],

            monthOptions: [
                { value: '01', text: 'Jan' },
                { value: '02', text: 'Feb' },
                { value: '03', text: 'Mar' },
                { value: '04', text: 'Apr' },
                { value: '05', text: 'May' },
                { value: '06', text: 'Jun' },
                { value: '07', text: 'Jul' },
                { value: '08', text: 'Aug' },
                { value: '09', text: 'Sep' },
                { value: '10', text: 'Oct' },
                { value: '11', text: 'Nov' },
                { value: '12', text: 'Dec' },
            ],
            quarterOptions: [
                { value: '01-03', text: 'Q1' },
                { value: '04-06', text: 'Q2' },
                { value: '07-09', text: 'Q3' },
                { value: '10-12', text: 'Q4' },
            ],

            selectedStartDate: null,
            selectedEndDate: null,
            
            selectedStartWeek: null,
            selectedEndWeek: null,
            
            // selectedStartMth: null,
            // selectedStartMthYear: null,
            // selectedEndMth: null,
            // selectedEndMthYear: null,

            // selectedStartQuarter: null,
            // selectedStartQuarterYear: null,
            // selectedEndQuarter: null,
            // selectedEndQuarterYear: null,

            // Default: 2017 till current time (in moment to feed datepicker)
            // selectedStartYear: 2017,
            // selectedEndYear: moment().format("YYYY"),

            // Format dates to feed to backend (default in days)
            formattedStartDate: moment().subtract(7, 'days').format("YYYY-MM-DD"),
            formattedEndDate: moment().format("YYYY-MM-DD"),

            ageRangeOptions:[
                { value: '1-20', text: '<20' },
                { value: '20-29', text: '20-29' },
                { value: '30-39', text: '30-39' },
                { value: '40-49', text: '40-49' },
                { value: '50-59', text: '50-59' },
                { value: '60-100', text: '>60'}
            ],

            //selectedAgeRange: ['1-20', '20-29', '20-29', '30-39', '40-49', '50-59', '60-100'],
            selectedAgeRange: [],

            genderOptions: [
                { value: '0', text: 'Female' },
                { value: '1', text: 'Male' },
            ],

            //selectedGender:  ['0', '1'], 
            selectedGender: [],

            salesChartData:{},
            basketChartData:{},
            tixChartData:{},

            totalSales: 0,
            noTransactions: 0,
            uniqueCustomers: 0,

            errorMessages: null,
            onClickGenerate: false,
            showloading: false,
        };
    }

    async componentDidMount(){
        const body = this.state.body;
        console.log(body)

        this.formatStartDate(this.state.selectedStartDate, null, true);
        this.formatEndDate(this.state.selectedEndDate, null, true);

        await this.getAllStores(body);
        await this.getAllCategories(body);
        await this.getAllProducts(body);
        //await this.checkIfAllCatSelected();
    }

    async getAllStores(body){
        let allStores = await this.callPostAPI(AVAILABLE_STORES, body)
        if(!allStores.error){
            let storeOptions = allStores.map((store) => {
                return {
                    value: store.outlet_code,
                    text: store.outlet_name,
                }
            }); 

            // let selectedStores = allStores.map((store) => {
            //     return store.outlet_code
            // })

            console.log("allStores: ", storeOptions)
            this.setState({ 
                storeOptions: storeOptions,
                //selectedStores: selectedStores
            });
        } 
    }

    async getAllCategories(body){
        let categoriesResult = await this.callPostAPI(CATEGORIES, body);

        if(!categoriesResult.error){
            let categoryOptions = categoriesResult.map((cat) => {
                return {
                    value: cat.product_category_name,
                    text: cat.product_category_name,
                }
            });

            // let allCatOption = {value: 0, text: "ALL CATEGORIES"}
            // categoryOptions.splice(0, 0, allCatOption)
            categoryOptions.unshift({
                value: 'All Categories',
                text: 'All Categories',
            })

            console.log("All pull: ", categoryOptions)
            this.setState({ 
                categoryOptions: categoryOptions,
                selectedCategories: ['All Categories']
            })

        }
    }

    async getAllProducts(body){
        let productsResult = await this.callPostAPI(PRODUCTS, body);

        if(!productsResult.error){
            let productOptions = productsResult.map((product) => {
                return {
                    label: product.product_name,
                    value: product.product_merchant_sku,
                }
            })
            console.log("All Products: ", productOptions);
            this.setState({ 
                productOptions: productOptions,
            });
        }
    }

    handleChangeStart(momentStartDate){
        this.formatStartDate(momentStartDate, null, false)         
    }

    handleChangeEnd(momentEndDate){
        this.formatEndDate(momentEndDate, null, false)
    }
                            
    // Filter based on start date selected
    // dateType: mth, year
    // format to send to backend: YYYY-MM-DD
    // isDefault: t/f (if date range should take in default values)

    formatStartDate(input, dateType, isDefault) {
        let durationType = this.state.selectedDuration;

        let selectedStartDate = this.state.selectedStartDate;
        let selectedStartWeek = this.state.selectedStartWeek;

        let formattedStartDate = this.state.formattedStartDate;

        if (durationType === "Daily"){                                      // Daily 
            if(isDefault){  
                selectedStartDate = moment().subtract(7, 'days'),
                formattedStartDate = selectedStartDate.format("YYYY-MM-DD")
            }else{
                selectedStartDate = input;                                  // moment
                formattedStartDate = input.format("YYYY-MM-DD");            // send to backend
            }
            this.setState({
                selectedStartDate: selectedStartDate
            })
        }else if (durationType === "Weekly"){                               // Weekly
            if(isDefault){  
                selectedStartWeek = moment().subtract(4, 'weeks').startOf('week');
                formattedStartDate = selectedStartWeek.format("YYYY-MM-DD");
            }else{
                selectedStartWeek = input;                                  // moment
                formattedStartDate = input.format("YYYY-MM-DD");            // send to backend
            }
            this.setState({
                selectedStartWeek: selectedStartWeek
            })
        }
        else if(durationType === "Monthly"){  
            if(isDefault){
                formattedStartDate = moment().subtract(2, 'months').format("YYYY-MM-01");
            }else {
                if(dateType === "mth"){
                    formattedStartDate = formattedStartDate.replace(formattedStartDate.substring(4,8), "-" + input + "-"); // set month
                    formattedStartDate = formattedStartDate.replace(formattedStartDate.substring(7,10), "-01");            // set date to 01
                }else if (dateType === "year"){
                    formattedStartDate = formattedStartDate.replace(formattedStartDate.substring(0,5), input + "-");       // set year
                }
            }       
        } else if(durationType === "Quarterly"){
            if(isDefault){
                let currentMth = parseInt(moment().format("MM"))
                let currentQuarter = parseInt(this.state.quarterPeriod[currentMth]);
                let currentYear = parseInt(moment().format("YYYY"));
                let startQuarter;
                let startYear = currentYear;

                if(currentQuarter - 2 <= 0){
                    startQuarter = currentQuarter - 2+4;
                    startYear = startYear - 1;
                }
                formattedStartDate = startYear + "-" + this.state.startOfMthInEachQuarter[startQuarter] + "-01";
                console.log(formattedStartDate)

            }else{
                if(dateType === "mth"){
                    input = input.substring(0, input.indexOf("-"));
                    formattedStartDate = formattedStartDate.replace(formattedStartDate.substring(4,8), "-" + input + "-"); // set month
                    formattedStartDate = formattedStartDate.replace(formattedStartDate.substring(7,10), "-01");            // set date to 01
                }else if (dateType === "year"){
                    formattedStartDate = formattedStartDate.replace(formattedStartDate.substring(0,5), input + "-");       // set year
                } 
            }
            
        }else if (durationType === "Yearly"){
            if(isDefault){
                formattedStartDate = "2017-01-01"
            }else{
                formattedStartDate = formattedStartDate.replace(formattedStartDate.substring(0,5), input + "-");       // set year
                formattedStartDate = formattedStartDate.replace(formattedStartDate.substring(4,8), "-01-");            // set month to 01
                formattedStartDate = formattedStartDate.replace(formattedStartDate.substring(7,10), "-01");            // set date to 01
            }
        }
        console.log("selected start date: ", formattedStartDate)
        
        this.setState({
            formattedStartDate: formattedStartDate
        });
    }

    // filter based on end date selected
    // backend format: YYYY-MM-DD
    // isDefault: t/f (if date range should take in default values)

    formatEndDate(input, dateType, isDefault) {
        //console.log("end input:", input)
        let durationType = this.state.selectedDuration;
        let selectedEndDate = this.state.selectedEndDate;
        let selectedEndWeek = this.state.selectedEndWeek;

        let formattedEndDate = this.state.formattedEndDate;

        if (durationType === "Daily"){     // Daily 
            if(isDefault){
                selectedEndDate = moment(),
                formattedEndDate = selectedEndDate.format("YYYY-MM-DD");
            }else{
                selectedEndDate = input;
                formattedEndDate = input.format("YYYY-MM-DD");
            }
            
            this.setState({
                selectedEndDate: selectedEndDate
            })
        } else if (durationType === "Weekly"){     // Weekly
            if(isDefault){
                selectedEndWeek = moment().endOf("week"),
                formattedEndDate = selectedEndWeek.format("YYYY-MM-DD");
            }else{
                selectedEndWeek = input;
                formattedEndDate = input.format("YYYY-MM-DD");
            }
            
            this.setState({
                selectedEndWeek: selectedEndWeek
            })

        } else if(durationType === "Monthly"){
            if(isDefault){
                let lastDayOfMth = this.state.daysInEachMth[parseInt(moment().format("MM"))]
                formattedEndDate = moment().format("YYYY-MM-" + lastDayOfMth);
            }
            if(dateType === "mth"){
                let lastDayOfMth = this.state.daysInEachMth[parseInt(input)];
                formattedEndDate = formattedEndDate.replace(formattedEndDate.substring(4,8), "-" + input + "-");       // set month
                formattedEndDate = formattedEndDate.replace(formattedEndDate.substring(7,10), "-" + lastDayOfMth);     // set date to end of mth
            }else if (dateType === "year"){
                formattedEndDate = formattedEndDate.replace(formattedEndDate.substring(0,5), input + "-");              // set year
            }
        } else if(durationType === "Quarterly"){
            if(isDefault){
                let currentMth = parseInt(moment().format("MM"))
                let currentQuarter = parseInt(this.state.quarterPeriod[currentMth]);
                let currentYear = parseInt(moment().format("YYYY"));
                let endMth = parseInt(this.state.startOfMthInEachQuarter[currentQuarter]) + 2;  // take last month of the quarter
                if (endMth < 10){
                    endMth = "0" + endMth; 
                }
                formattedEndDate = currentYear + "-" + endMth + "-" + this.state.daysInEachMth[parseInt(endMth)];
                console.log(formattedEndDate)
            }else{
                if(dateType === "mth"){
                    input = input.substring(input.indexOf("-") + 1, input.length)
                    let lastDayOfMth = this.state.daysInEachMth[parseInt(input)];
                    formattedEndDate = formattedEndDate.replace(formattedEndDate.substring(4,8), "-" + input + "-");       // set month to end of quarter
                    formattedEndDate = formattedEndDate.replace(formattedEndDate.substring(7,10), "-" + lastDayOfMth);     // set date to end of mth
                }else if (dateType === "year"){
                    formattedEndDate = formattedEndDate.replace(selectedEndDate.substring(0,5), input + "-");       // set year
                }
            }
        }else if (durationType === "Yearly"){
            let lastDayOfMth = this.state.daysInEachMth[12];
            if(isDefault){
                formattedEndDate = moment().format("YYYY") + "-12-" + lastDayOfMth
            } else{
                formattedEndDate = formattedEndDate.replace(formattedEndDate.substring(0,5), input + "-");          // set year
                formattedEndDate = formattedEndDate.replace(formattedEndDate.substring(4,8), "-12-");               // set month to 12
                formattedEndDate = formattedEndDate.replace(formattedEndDate.substring(7,10), "-" + lastDayOfMth);  // set date to 31
            }
        }
        console.log("selected end date: " , formattedEndDate)
        
        this.setState({
            formattedEndDate: formattedEndDate
        });
    }

    isStartOfWeek = date => {
        const day = date.day();
        return day === 1;
    };

    isEndOfWeek = date => {
        const day = date.day();
        return day === 0;
    };

    prepareFilterBody(){
        const body = {
            "merchant_name": this.state.body.merchant_name,
            "filter": this.state.selectedDuration,
            "start": this.state.selectedStartDate,
            "end": this.state.selectedEndDate,
            "outlets": this.state.selectedStores,
            "categories": this.state.selectedCategories,
            "products": this.state.formatSelectedProducts,
            "age_range": this.state.selectedAgeRange,
            "gender": this.state.selectedGender
        }
        return body;
    }

    async generateData() {
        
        // Format selected products
        var formatSelectedProducts = [];
        if(this.state.selectedProducts.length > 0){
            formatSelectedProducts = this.state.selectedProducts.map((selected) => {
                return selected.value
            })
            await this.setState({formatSelectedProducts: formatSelectedProducts});
        }
        
        if(this.state.selectedStores.length === 0) {
            let allStores = this.state.storeOptions.map((stores) => {
                return stores.value
            })
            await this.setState({
                selectedStores: allStores
            })
        }
        
        if (this.state.selectedCategories.length === 0 && this.state.formatSelectedProducts.length === 0) {
            let allCat = this.state.categoryOptions.map((cat) => {
                return cat.value
            })
            await this.setState({
                selectedCategories: allCat
            })
        }

        if(this.state.selectedAgeRange.length === 0) {
            let allAgeRanges = this.state.ageRangeOptions.map((ageRange) => {
                return ageRange.value
            })
            await this.setState({
                selectedAgeRange: allAgeRanges
            })
        }
        
        if (this.state.selectedGender.length === 0) {
            let allGenders = this.state.genderOptions.map((gender) => {
                return gender.value
            })
            await this.setState({
                selectedGender: allGenders
            })
        }

        console.log("Selected stores:", this.state.selectedStores)
        console.log("Selected Categories:", this.state.selectedCategories)
        console.log("Selected Products:", this.state.formatSelectedProducts)
        console.log("Selected Age range:", this.state.selectedAgeRange)
        console.log("Selected Genders:", this.state.selectedGender)
        console.log("Selected duration:", this.state.selectedDuration)
        console.log("Selected startDate:", this.state.formattedStartDate)
        console.log("Selected endDate:", this.state.formattedEndDate)
            
        const body = this.prepareFilterBody();

        let salesAnalysisResult = await this.callPostAPI(SALES_ANALYSIS, body);
        console.log("salesAnalysisResult: ", salesAnalysisResult)
        if(!salesAnalysisResult.error){

            // Remove the set interval when the this form already integrated
            setInterval(
                this.setState({
                    salesChartData: salesAnalysisResult.overall_sales_graph,
                    basketChartData: salesAnalysisResult.average_basket_graph,
                    tixChartData: salesAnalysisResult.average_ticket_graph,
                    
                    totalSales: salesAnalysisResult.sales_analysis_summary.total_sales,
                    noTransactions: salesAnalysisResult.sales_analysis_summary.no_of_transactions,
                    uniqueCustomers: salesAnalysisResult.sales_analysis_summary.unique_customers,
    
                    onClickGenerate: true
                })
                , 9000);

        }
    }
    

    async callGetAPI(api){
        let result = {};
        await fetch(API_SERVER_URL + api,{
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
        .then((response) => response.json())
        .then((responseJson)=>{
            result = responseJson;
        })

        return result;
    }

    async callPostAPI(api, body){
        let result = {};
        
        await fetch(API_SERVER_URL + api,{
            method: 'POST', 
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
        .then((response) => response.json())
        .then((responseJson)=>{
            result = responseJson;
        })

        return result;
    }

    async checkIfAllCatSelected(){
        var isAllCatSelected = this.state.isAllCatSelected;

        if(isAllCatSelected){
            let catOptions = this.state.categoryOptions;
            let newCatOptions = [];
            newCatOptions.push(catOptions[0])
            
            await this.setState({
                categoryOptions: newCatOptions
            })
            //console.log(this.state.categoryOptions)
        }else{
            await this.getAllCategories(this.state.body);
            //console.log(this.state.categoryOptions)
        }
        
    }

    render() {
        return (
            <div className="analysis">
                <div className="analysis-options">
                    <div className="input-container full">
                        <p className="label">Store</p>
                        <Dropdown className="multiple-dropdown-selector" placeholder="Select store(s)"
                            selection value={this.state.selectedStores}
                            multiple search selection 
                            closeOnChange = {true}
                            options={this.state.storeOptions} 
                            onChange={
                                async (event, data) => {
                                    await this.setState({
                                        selectedStores: data.value
                                    })
                                }
                            }
                        />
                    </div>
                        
                    <div className="input-container full">
                        <p className="label">Category</p>
                        <Dropdown className="multiple-dropdown-selector" placeholder="Select Category"
                            selection value={this.state.selectedCategories}
                            multiple search selection 
                            closeOnChange = {true}
                            options={this.state.categoryOptions} 
                            onChange={
                                async (event, data) => {
                                    
                                    if(data.value.indexOf('All Categories') > -1){
                                        await this.setState({ 
                                            selectedCategories: ['All Categories']
                                        })
                                    }else{
                                        if(this.state.selectedCategories.indexOf(data.value) == -1){

                                            if(this.state.selectedCategories.indexOf('All Categories') == -1 || this.state.selectedCategories.length > data.value.length){
                                                await this.setState({ 
                                                    selectedCategories: data.value 
                                                })
                                            }
    
                                        }
                                    }
                                }
                            }
                        />
                    </div>

                    <div className="input-container full">
                        <p className="label">Product</p>
                        <Select name="form-field-name" placeholder="Select Product(s)" 
                            closeOnSelect={true}
                            clearable={true}
                            disabled={false}
                            multi={true}
                            options={this.state.productOptions} 
                            value={this.state.selectedProducts}
                            onChange={(selected)=>{
                                this.setState({selectedProducts: selected})
                                }
                            }
                            searchable={true}
                            labelKey='label'
                            valueKey='label'
                        />
                    </div>

                    <div className="input-container">
                        <p className="label">Duration</p>
                        <Dropdown className="dropdown-selector" 
                            placeholder="Select Duration" 
                            defaultValue={'Daily'} 
                            selection 
                            options={this.state.durationOptions} 
                            closeOnChange = {true}
                            onChange={async (event, data) => { 
                                await this.setState({
                                    selectedDuration: data.value,
                                })
                                await this.formatStartDate(null, null, true)
                                await this.formatEndDate(null, null, true)
                            }}
                        />
                    </div>

                    {/* Daily State */}
                    { this.state.selectedDuration === 'Daily' ? (
                        <div className="input-container date">
                            <p className="label">Start Date</p>
                            <DatePicker className="datepicker date-selector" 
                                selected={this.state.selectedStartDate} selectsStart
                                //startDate={this.state.selectedStartDate}
                                //endDate={this.state.selectedEndDate}
                                onChange={this.handleChangeStart.bind(this)}
                                isClearable={true}
                                showYearDropdown={true}
                                style={{flex: 1}}
                                placeholderText="Start Date"
                                locale="en-gb"
                            />
                        </div>
                    ) : (null)}

                    { this.state.selectedDuration === 'Daily' ? (
                        <div className="input-container date">
                            <p className="label">End Date</p>
                            <DatePicker className="datepicker date-selector" selectsEnd
                                selected={this.state.selectedEndDate} 
                                minDate = {this.state.selectedStartDate}
                                //startDate={this.state.selectedStartDate} 
                                //endDate={this.state.selectedEndDate}
                                onChange={this.handleChangeEnd.bind(this)}
                                isClearable={true}
                                showYearDropdown={true}
                                placeholderText="End Date"
                                locale="en-gb"
                            />
                        </div>
                    ) : (null)}

                    {/* Weekly State */}
                    { this.state.selectedDuration === 'Weekly' ? (
                        <div className="input-container date">
                            <p className="label">Start Week</p>
                            <DatePicker
                                className="datepicker date-selector" selectsStart
                                selected={this.state.selectedStartWeek}
                                //startDate={this.state.startDate}
                                //endDate={this.state.endDate}
                                onChange={this.handleChangeStart.bind(this)}
                                isClearable={true}
                                showYearDropdown={true}
                                style={{flex: 1}}
                                filterDate={this.isStartOfWeek}
                                placeholderText="Start Week"
                                locale="en-gb"
                            />
                        </div>
                    ) : (null)}

                    { this.state.selectedDuration === 'Weekly' ? (
                        <div className="input-container date">
                            <p className="label">End Week</p>
                            <DatePicker
                                className="datepicker date-selector" selectsEnd
                                selected={this.state.selectedEndWeek}
                                minDate = {this.state.selectedStartWeek}
                                //startDate={this.state.startDate} 
                                //endDate={this.state.endDate}
                                onChange={this.handleChangeEnd.bind(this)}
                                isClearable={true}
                                showYearDropdown={true}
                                filterDate={this.isEndOfWeek}
                                placeholderText="End Week"
                                locale="en-gb"
                            />
                        </div>
                    ) : (null)}

                    {/* Monthly State */}
                    { this.state.selectedDuration === 'Monthly' ? (
                        <div className="input-container date">
                            <p className="label">Start Month</p>
                            <div className="month-selector">
                                <Dropdown className="month" compact placeholder="Month"
                                    options={this.state.monthOptions} 
                                    defaultValue={moment().subtract(2,'months').format("MM")}
                                    //selected={this.state.selectedStartMth}
                                    closeOnChange = {true}
                                    style={{display: 'flex', alignItems: 'center', justifyContent: 'center', padding: '0px 10px'}}
                                    onChange={
                                        async(event, data) => {
                                            console.log(data)
                                            this.formatStartDate(data.value, "mth", false)
                                        }      
                                    }
                                />

                                <Dropdown className="dropdown-selector" placeholder="Year"
                                    selection 
                                    options={this.state.yearOptions} 
                                    defaultValue={moment().subtract(2,'months').format("YYYY")}
                                    closeOnChange = {true}
                                    onChange={
                                        async(event, data) => {
                                            console.log(data)
                                            this.formatStartDate(data.value, "year", false)
                                        }      
                                    }
                                />
                            </div>
                        </div>
                    ) : (null)}

                    { this.state.selectedDuration === 'Monthly' ? (
                        <div className="input-container date">
                            <p className="label">End Month</p>
                            <div className="month-selector">
                                <Dropdown className="month" compact placeholder="Month"
                                    options={this.state.monthOptions}
                                    defaultValue={moment().format("MM")}
                                    //selected={this.state.selectedEndMth}
                                    closeOnChange = {true}
                                    style={{display: 'flex', alignItems: 'center', justifyContent: 'center', padding: '0px 10px'}}
                                    onChange={
                                        async(event, data) => {
                                            this.formatEndDate(data.value, "mth", false)
                                        }    
                                    }
                                />

                                <Dropdown className="dropdown-selector" placeholder="Year"
                                    selection 
                                    options={this.state.yearOptions} 
                                    defaultValue={moment().format("YYYY")}
                                    closeOnChange = {true}
                                    onChange={
                                        async(event, data) => {
                                            this.formatEndDate(data.value, "year", false)
                                        }    
                                    }
                                />
                            </div>
                        </div>
                    ) : (null)}

                    {/* Quarterly State */}
                    { this.state.selectedDuration === 'Quarterly' ? (
                        <div className="input-container date">
                            <p className="label">Start Quarter</p>
                            <div className="quarter-selector">
                                <Dropdown className="quarter" compact placeholder="Quarter"
                                    options={this.state.quarterOptions} 
                                    defaultValue = {moment().subtract(2, 'quarter').startOf('quarter').format("MM") + "-" + moment().subtract(2, 'quarter').endOf('quarter').format("MM")}
                                    closeOnChange = {true}                                   
                                    style={{display: 'flex', alignItems: 'center', justifyContent: 'center', padding: '0px 10px'}}
                                    onChange={
                                        async(event, data) => {
                                            this.formatStartDate(data.value, "mth", false)
                                        }    
                                    }
                                />

                                <Dropdown className="dropdown-selector" placeholder="Year"
                                    selection 
                                    options={this.state.yearOptions} 
                                    defaultValue = {moment().subtract(2, 'quarter').format("YYYY")}
                                    closeOnChange = {true}
                                    onChange={
                                        async(event, data) => {
                                            this.formatStartDate(data.value, "year", false)
                                        }    
                                    }
                                />
                            </div>
                        </div>
                    ) : (null)}

                    { this.state.selectedDuration === 'Quarterly' ? (
                        <div className="input-container date">
                            <p className="label">End Quarter</p>
                            <div className="quarter-selector">
                                <Dropdown className="quarter" compact placeholder="Quarter"
                                    options={this.state.quarterOptions} 
                                    defaultValue = {moment().startOf('quarter').format("MM") + "-" + moment().endOf('quarter').format("MM")}
                                    closeOnChange = {true}
                                    style={{display: 'flex', alignItems: 'center', justifyContent: 'center', padding: '0px 10px'}}
                                    onChange={
                                        async(event, data) => {
                                            this.formatEndDate(data.value, "mth", false)
                                        }    
                                    }
                                />

                                <Dropdown className="dropdown-selector" placeholder="Year"
                                    selection 
                                    options={this.state.yearOptions} 
                                    defaultValue = {moment().format("YYYY")}
                                    closeOnChange = {true}
                                    onChange={
                                        async(event, data) => {
                                            this.formatEndDate(data.value, "year", false)
                                        }    
                                    }
                                />
                            </div>
                        </div>
                    ) : (null)}

                    {/* Yearly State */}
                    { this.state.selectedDuration === 'Yearly' ? (
                        <div className="input-container date">
                            <p className="label">Start Year</p>
                            <Dropdown className="dropdown-selector" placeholder="Year"
                                selection 
                                options={this.state.yearOptions} 
                                defaultValue = {"2017"}
                                closeOnChange = {true}
                                onChange={
                                    async(event, data) => {
                                        this.formatStartDate(data.value, "year", false)
                                    }    
                                }
                            />
                        </div>
                    ) : (null)}

                    { this.state.selectedDuration === 'Yearly' ? (
                        <div className="input-container date">
                            <p className="label">End Year</p>
                            <Dropdown className="dropdown-selector" placeholder="Year"
                                selection 
                                options={this.state.yearOptions} 
                                defaultValue = {moment().format("YYYY")}
                                closeOnChange = {true}
                                onChange={
                                    async(event, data) => {
                                        this.formatEndDate(data.value, "year", false)
                                    }    
                                }
                            />
                        </div>
                    ) : (null)}

                     <div className="input-container">
                        <p className="label">Age Range</p>
                        <Dropdown className="multiple-dropdown-selector" placeholder="Select Age Range" 
                            multiple search selection  
                            selection value={this.state.selectedAgeRange}
                            options={this.state.ageRangeOptions} 
                            closeOnChange = {true}
                            onChange={
                                async (event, data) => {
                                    await this.setState({ 
                                        selectedAgeRange: data.value
                                    })
                                }
                            }
                        />
                    </div> 

                    <div className="input-container">
                        <p className="label">Gender</p>
                        <Dropdown className="multiple-dropdown-selector" placeholder="Select Gender" 
                            multiple search selection  
                            selection value={this.state.selectedGender}
                            options={this.state.genderOptions} 
                            closeOnChange = {true}
                            onChange={
                                async (event, data) => {
                                await this.setState({ 
                                    selectedGender: data.value
                                })
                            }}
                        />
                    </div> 

                    <div className="input-container">
                        <Button
                            className="button"
                            id="data-button"
                            onClick={
                                async (event, data) => {
                                    this.generateData()
                                }
                            }>
                            View Data
                        </Button>
                    </div>

                    <div className="input-container">      
                        {this.state.errorMessages}                  
                    </div> 

                    <div className="input-container">                      
                    </div> 
                </div>
                
                { this.state.showloading? 
                    <img style={{position: 'absolute', left: '50%', width: '20%', top: '50%'}} src={spinner} alt=""/>
                :
                    null
                }

                {/* Generate data only if there is filter */}
                 { this.state.onClickGenerate ? (
                    <div>
                        <div>
                            <Chart 
                                data={this.state.salesChartData}
                                title="Overall Sales"
                                dcm={2}
                            />
                        </div>

                        <div style={{display: 'flex', flex: 1, height: '320px'}}>
                            <div style={{flex: 1, height: '100%'}}>
                                <Chart 
                                    data={this.state.basketChartData}
                                    title="Average Basket"
                                    height={100}
                                    dcm={2}
                                />
                            </div>
                            <div style={{flex: 1, height: '100%'}}>
                                <Chart 
                                    data={this.state.tixChartData}
                                    title="Average Ticket"
                                    height={100}
                                    dcm={0}
                                />
                            </div>
                        </div>

                        <div className="analysis-stats">
                            <div className="card" id="avg-ticket-size">
                                <h3 className="title">Total Sales</h3>
                                {/* <h2 className="value">${Number(this.state.totalSales).toFixed(2)}</h2> */}
                                <h2 className="value">
                                    ${this.state.totalSales > 0 ?
                                        this.state.totalSales.toLocaleString(navigator.language, { minimumFractionDigits: 2 })
                                        :
                                        "$0.00"
                                    }
                                </h2>
                                
                            </div>
                            <div className="card" id="avg-basket-size">
                                <h3 className="title">No. of Transactions</h3>
                                <h2 className="value">{Number(this.state.noTransactions).toFixed(0)}</h2>
                                
                            </div>
                            <div className="card" id="no-of-transactions">
                                <h3 className="title">Unique Customers</h3>
                                <h2 className="value">{Number(this.state.uniqueCustomers).toFixed(0)}</h2>
                            </div>
                        </div>  
                    </div>
                ) : null }        
            </div>
        );
    }
}

export default Analysis;
