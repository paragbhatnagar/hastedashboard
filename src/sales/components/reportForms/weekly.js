import React, { Component } from 'react';
import { Icon, Button, Dropdown, Input } from 'semantic-ui-react';

//import components

// import styling and assets
import '../../../assets/css/sales.css';

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/en-gb';

export default class Weekly extends Component {
    //forDROPDOWN
    isStartOfWeek = date => {
        const day = date.day();
        return day === 1;
    };

    isEndOfWeek = date => {
        const day = date.day();
        return day === 0;
    };

    render() {
        return(
            <div style={{display: "flex", justifyContent:"row"}}>
                <div className="input-container date" style={{flex:1, paddingRight:30}} >
                    <p className="label">Start Week</p>
                    <DatePicker
                        className="datepicker date-selector" selectsStart
                        dateFormat="YYYY/MM/DD"
                        selected={this.props.startDateWeek}
                        startDate={this.props.startDateWeek}
                        endDate={this.props.endDateWeek}
                        onChange={this.props.handleWeeklyStartDateChange}
                        style={{flex: 1}}
                        filterDate={this.isStartOfWeek}
                        locale="en-gb"
                    />
                </div>
                <div className="input-container date" style={{flex:1, paddingRight: 20}}>
                    <p className="label">End Week</p>
                    <DatePicker
                        className="datepicker date-selector" selectsEnd
                        dateFormat="YYYY/MM/DD"
                        selected={this.props.endDateWeek}
                        startDate={this.props.startDateWeek}
                        endDate={this.props.endDateWeek}
                        onChange={this.props.handleWeeklyEndDateChange}
                        filterDate={this.isEndOfWeek}
                        locale="en-gb"
                    />
                </div>
            </div>
        )


    }
}