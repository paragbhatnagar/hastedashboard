import React, { Component } from 'react';
import { Icon, Button, Dropdown, Input } from 'semantic-ui-react';

//import components

// import styling and assets
import '../../../assets/css/sales.css';

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/en-gb';

export default class Yearly extends Component {
    render(){
        return(
            <div style={{display:"flex", justifyContent:"row"}}>
                {/*Start year*/}
                <div className="input-container date" style={{flex:1, paddingRight:30}}>
                    <p className="label">Start Year</p>
                    <Input className="year" type='number' fluid style={{flex: 1, width:100}}
                           defaultValue = {this.props.startYear}
                           max={this.props.endYear}
                           onChange={(event, data) => {this.props.handleStartYearChange(data.value)}}
                    />
                </div>

                {/*End year*/}
                <div className="input-container date" style={{flex:1, paddingRight:30}}>
                    <p className="label">End Year</p>

                    <Input className="year" type='number' fluid style={{flex: 1, width:100}}
                           defaultValue = {this.props.endYear}
                           min = {this.props.startYear}
                           onChange={(event, data) => this.props.handleEndYearChange(data.value)}
                    />
                </div>
            </div>
        )
    }
}

