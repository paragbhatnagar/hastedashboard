import React, { Component } from 'react';
import { Icon, Button, Dropdown, Input } from 'semantic-ui-react';

//import components

// import styling and assets
import '../../../assets/css/sales.css';

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/en-gb';

export default class Daily extends Component {

    render() {
        return (
            <div style={{display: "flex", justifyContent: "row"}}>
                {/*Start Day*/}
                <div className="input-container date" style={{flex: 1, paddingRight: 30}}>
                    <p className="label">Start Date</p>
                    <DatePicker
                        className="datepicker date-selector" selectsEnd
                        dateFormat="YYYY/MM/DD"
                        selected={this.props.startDateDaily}
                        startDate={this.props.startDateDaily}
                        endDate={this.props.endDateDaily}
                        onChange={this.props.handleDailyStartDateChange}
                        locale="en-gb"
                    />
                </div>
                {/*End Day*/}
                <div className="input-container date" style={{flex: 1, paddingRight: 30}}>
                    <p className="label">End Date</p>
                    <DatePicker
                        className="datepicker date-selector" selectsEnd
                        dateFormat="YYYY/MM/DD"
                        selected={this.props.endDateDaily}
                        startDate={this.props.startDateDaily}
                        endDate={this.props.endDateDaily}
                        onChange={this.props.handleDailyEndDateChange}
                        locale="en-gb"
                    />
                </div>

            </div>
        )
    }
}
