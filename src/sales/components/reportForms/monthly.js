import React, { Component } from 'react';
import { Icon, Button, Dropdown, Input } from 'semantic-ui-react';

//import components

// import styling and assets
import '../../../assets/css/sales.css';

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/en-gb';

export default class Monthly extends Component {

    render() {
        return (
            <div style={{display: "flex", justifyContent: "row"}}>
                {/*Month start*/}
                <div className="input-container date" style={{flex: 1, paddingRight: 30}}>
                    <p className="label">From</p>
                    <div className="month-selector" style={{display: 'flex', flexDirection: 'row'}}>
                        <Dropdown className="month"
                                  compact options={this.props.monthOptions}
                                  defaultValue={this.props.startUnitMonth}
                                  style={{
                                      display: 'flex', alignItems: 'center',
                                      justifyContent: 'center', padding: '0px 10px'
                                  }}
                                  onChange={(event, data) => {
                                      this.props.handleStartMonthChange(data.value)
                                  }}
                        />
                        <Input className="year" type='number' fluid style={{flex: 1, width: 100}}
                               defaultValue={this.props.startYear} max = {this.props.endYear}
                               onChange={(event, data) => this.props.handleStartYearChange(data.value)}
                        />
                    </div>
                </div>
                {/*Month end*/}
                <div className="input-container date" style={{flex: 1, paddingRight: 30}}>
                    <p className="label">To </p>
                    <div className="month-selector" style={{display: 'flex', flexDirection: 'row'}}>
                        <Dropdown className="month"
                                  compact options={this.props.monthOptions}
                                  defaultValue={this.props.endUnitMonth}
                                  style={{
                                      display: 'flex', alignItems: 'center',
                                      justifyContent: 'center', padding: '0px 10px'
                                  }}
                                  onChange={(event, data) => this.props.handleEndMonthChange(data.value)}
                        />
                        <Input className="year" type='number' fluid style={{flex: 1, width: 100}}
                               defaultValue={this.props.endYear} min = {this.props.startYear}
                               onChange={(event, data) => this.props.handleEndYearChange(data.value)}
                        />
                    </div>
                </div>
            </div>
        )
    }
}