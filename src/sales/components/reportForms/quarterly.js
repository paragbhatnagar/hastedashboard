import React, { Component } from 'react';
import { Icon, Button, Dropdown, Input } from 'semantic-ui-react';

//import components

// import styling and assets
import '../../../assets/css/sales.css';

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/en-gb';

export default class Quarterly extends Component {

    render() {
        return (
            <div style={{display: "flex", justifyContent: "row"}}>
                {/*Start quarter*/}
                <div className="input-container date" style={{flex: 1, paddingRight: 30}}>
                    <p className="label">Start Quarter</p>
                    <div className="quarter-selector" style={{display: 'flex', flexDirection: 'row'}}>
                        <Dropdown className="quarter"
                                  compact options={this.props.quarterOptions}
                                  defaultValue={this.props.startQuarter}
                                  style={{
                                      display: 'flex', alignItems: 'center',
                                      justifyContent: 'center', padding: '0px 10px'
                                  }}
                                  onChange={(event, data) => this.props.handleStartQuarterChange(data.value)}
                        />
                        <Input className="year" type='number' fluid
                               defaultValue={this.props.startYear} style={{flex: 1, width: 100}}
                               max = {this.props.endYear}
                               onChange={(event, data) => this.props.handleStartYearChange(data.value)}
                        />
                    </div>
                </div>
                {/*End quarter*/}
                <div className="input-container date" style={{flex: 1, paddingRight: 30}}>
                    <p className="label">End Quarter</p>
                    <div className="quarter-selector" style={{display: 'flex', flexDirection: 'row'}}>
                        <Dropdown className="quarter"
                                  compact options={this.props.quarterOptions}
                                  defaultValue={this.props.endQuarter}
                                  style={{
                                      display: 'flex', alignItems: 'center',
                                      justifyContent: 'center', padding: '0px 10px'
                                  }}
                                  onChange={(event, data) => this.props.handleEndQuarterChange(data.value)}
                        />
                        <Input className="year" style={{flex: 1, width: 100}} type='number' fluid
                               defaultValue={this.props.endYear} min = {this.props.startYear}
                               onChange={(event, data) =>  this.props.handleEndYearChange(data.value)}
                        />
                    </div>
                </div>
            </div>
        )

    }
}