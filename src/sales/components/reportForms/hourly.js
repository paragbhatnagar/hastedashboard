import React, { Component } from 'react';
import { Icon, Button, Dropdown, Input } from 'semantic-ui-react';

//import components

// import styling and assets
import '../../../assets/css/sales.css';

import DatePicker from 'react-datepicker';
import moment from 'moment';
import 'moment/locale/en-gb';

export default class Hourly extends Component {

    render() {
        return (
            <div className="input-container date" style={{flex: 1}}>
                <p className="label"> Day </p>
                <DatePicker
                    className="datepicker date-selector" selectsEnd
                    dateFormat="YYYY/MM/DD"
                    selected={this.props.hourlyDate}
                    onChange={this.props.handleHourlyDateChange}
                    locale="en-gb"
                />
            </div>
        )
    }
}