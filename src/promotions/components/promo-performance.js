import React, { Component } from 'react';
import {Icon, Button, Dropdown, Card, Grid, Image, Table, Divider, Pagination} from 'semantic-ui-react'

//import components
import SidebarMenu from '../../components/sidebar';
import Navbar from '../../components/navbar';

// import styling and assets
import '../../assets/css/sales.css';
import '../../assets/css/App.css';
import moment from "moment";
import {API_SERVER_URL, AVAILABLE_STORES} from "../../api";


class PromoPerformance extends Component{
    constructor(props){
        super(props);
        this.state={
            data:this.props.data,
            curActivePage:1
        }
    }

    handlePageChange = (e, { activePage }) => this.setState({ curActivePage: activePage })

    componentWillReceiveProps(nextProps) {
        this.setState({data: nextProps.data});
    }

    async callPostAPI(api, body){
        let result = {};

        await fetch(API_SERVER_URL + api,{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
            .then((response) => response.json())
            .then((responseJson)=>{
                result = responseJson;
            })

        return result;
    }
/*.filter((curData, i)=>{
    const lim = 4*this.state.curActivePage
    const checker = i+1
    return checker===lim-3
||checker===lim-2
||checker===lim-1
||checker===lim
})*/
render(){
        return(
            <div className="table-wrapper">
                {/* <div className="chart-header">
                    <div style={{display: 'inline'}}>
                        <h3 className="chart-title">Promotions</h3>
                    </div>
                </div> */}

                <Table celled className="table">
                    <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Product</Table.HeaderCell>
                        <Table.HeaderCell>Quantity</Table.HeaderCell>
                        <Table.HeaderCell>Promo Type</Table.HeaderCell>
                        <Table.HeaderCell>Amount</Table.HeaderCell>
                        <Table.HeaderCell>Start Date</Table.HeaderCell>
                        <Table.HeaderCell>End Date</Table.HeaderCell>
                        <Table.HeaderCell>Promo Sales</Table.HeaderCell>
                        <Table.HeaderCell>Store Name</Table.HeaderCell>
                    </Table.Row>
                    </Table.Header>

                    <Table.Body>
                        {console.log(this.state.data)}
                        {this.state.data===undefined || this.state.data===null || this.state.data.length===0?
                            <Table.Row>
                                <Table.Cell>There is no data available</Table.Cell>
                            </Table.Row>
                        :
                             this.state.data
                            .map((curData) =>{
                                return(
                                    <Table.Row>
                                        <Table.Cell>{curData.promotion_name}</Table.Cell>
                                        <Table.Cell>{curData.total_qty}</Table.Cell>
                                        <Table.Cell>{curData.promotion_type}</Table.Cell>
                                        <Table.Cell>{curData.discount_amount}</Table.Cell>
                                        <Table.Cell>{curData.start_date}</Table.Cell>
                                        <Table.Cell>{curData.end_date}</Table.Cell>
                                        <Table.Cell>${Number(curData.total_sales).toFixed(2)}</Table.Cell>
                                        <Table.Cell>{curData.outlet_name}</Table.Cell>
                                    </Table.Row>
                                )
                            }) 
                        }
                    </Table.Body>

                    {/* <Table.Footer>
                    <Table.Row>
                        <Table.HeaderCell colSpan='4'>
                         <Pagination 
                            defaultActivePage={1} 
                            totalPages={this.props.data.length/10>Math.floor(this.props.data.length/4)?
                                Math.floor(this.props.data.length/10)+1
                                :
                                Math.floor(this.props.data.length/10)} 
                            siblingRange={0}
                            lastItem={null}
                            firstItem={null}
                            onPageChange={this.handlePageChange}
                        /> 
                        </Table.HeaderCell>
                    </Table.Row>
                    </Table.Footer> */}
                </Table>

            </div>
        )
    }
}
export default PromoPerformance;