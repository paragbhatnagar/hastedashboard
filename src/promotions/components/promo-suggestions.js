import React, { Component } from 'react';

import {Segment, Card, Grid, Image, Icon, Button, Label, Table} from "semantic-ui-react";
import { Slider } from 'react-semantic-ui-range';

import {API_SERVER_URL, DEMOGRAPHICS_SUMMARY} from "../../api";

import '../../assets/css/shopper.css';
import pointer from '../../assets/img/pointer.png';

class PromoSuggestions extends Component{
    constructor(props){
        super(props);
        this.state = {
            selectedAge: null,
            avg_basket_size: 10.23,
            avg_ticket_size: 7,
            top_category: "Dairy",
            top_promotion: "Buy 1 get 1 Oreo",
            frequent_visit: "Monday, 11pm - 2pm for 12 mins",
            value1: 4,
            value2: 4,
            value3: 4,
            value: 0
        }

    }
//handlers
    async handleClick(active_age){
        await this.changeAge(active_age);

       await this.getResults();

    }

    async changeAge(active_age){
        this.setState({selectedAge:active_age});
        return this.state.selectedAge;
    }

    async getResults(){
        let result = await this.callPostAPI(DEMOGRAPHICS_SUMMARY, {
            ageGroup: this.state.selectedAge
        });
        //handle result
    }


    async callPostAPI(api, body){
        let result = {};
        console.log(""+API_SERVER_URL + api);
        await fetch(API_SERVER_URL + api,{

            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
            .then((response) => response.json())
            .then((responseJson)=>{
                result = responseJson;
            })

        return result;
    }

    render(){
        return (
            <div>
                <div style={{background:'white',alignSelf:'center',textAlign:'center',margin:'3px',height:'40px'}}><h2 className="title" style={{color:'black',alignSelf:'center'}}>Work in Progress</h2></div>
                <Grid style={{backgroundColor:'white',margin:'15px'}}>
                <Grid.Column width={16}>
                    <Segment className="affinity-card">
                        <h2>Choose shopper profiles</h2>
                        <div className="shopper-checkbox-list">
                            <label className="checkboxcontainer">
                                <input type="checkbox"/>
                                <span className="checkmark"></span>
                                All Shopper(s)
                            </label>
                            <label className="checkboxcontainer">
                                <input type="checkbox"/>
                                <span className="checkmark"></span>
                                Shopper A
                            </label>

                            <label className="checkboxcontainer">
                                <input type="checkbox"/>
                                <span className="checkmark"></span>
                                Shopper B
                            </label>

                            <label className="checkboxcontainer">
                                <input type="checkbox"/>
                                <span className="checkmark"></span>
                                Shopper C
                            </label>

                            <label className="checkboxcontainer">
                                <input type="checkbox"/>
                                <span className="checkmark"></span>
                                Shopper D
                            </label>
                        </div>
                        <div style={{display: 'flex', width: '100%', alignItems: 'flex-end', paddingTop: '20px', paddingBottom: '20px'}}>
                            <h3 style={{paddingRight: '20px', paddingBottom: '5px'}}>Accuracy</h3>
                            <div className="slider-card">
                                <Label color="grey" className="slider-value">{this.state.value1}</Label>
                                <p className="slider-container">
                                <Slider color="grey" inverted={false}
                                    settings={{ start: this.state.value1, min:0, max:10, step:1,
                                    onChange: (value) => { this.setState({ value1: value })}
                                }}/>
                                </p>
                            </div>
                        </div>
                        <div style={{display: 'flex', width: '100%', alignItems: 'flex-end', paddingTop: '20px', paddingBottom: '20px'}}>
                            <h3 style={{paddingRight: '20px', paddingBottom: '5px'}}>Diversity</h3>
                            <div className="slider-card">
                                <Label color="grey" className="slider-value">{this.state.value2}</Label>
                                <p className="slider-container">
                                <Slider color="grey" inverted={false}
                                    settings={{ start: this.state.value1, min:0, max:10, step:1,
                                    onChange: (value) => { this.setState({ value2: value })}
                                }}/>
                                </p>
                            </div>
                        </div>
                        <div style={{display: 'flex', width: '100%', alignItems: 'flex-end', paddingTop: '20px', paddingBottom: '20px'}}>
                            <h3 style={{paddingRight: '20px', paddingBottom: '5px'}}>Coverage</h3>
                            <div className="slider-card">
                                <Label color="grey" className="slider-value">{this.state.value3}</Label>
                                <p className="slider-container">
                                <Slider color="grey" inverted={false}
                                    settings={{ start: this.state.value1, min:0, max:10, step:1,
                                    onChange: (value) => { this.setState({ value3: value })}
                                }}/>
                                </p>
                            </div>
                        </div>
                        
                        <Button
                            className="button"
                            id="data-button" style={{width: 200, backgroundColor: '#FB8361', color: 'white' }} 
                            onClick={()=>{}}
                            >
                            GENERATE PROMOTIONS
                        </Button>

                        <h2>Result</h2>
                        <Table celled className="table">
                            <Table.Header>
                                <Table.Row>
                                    <Table.HeaderCell>Promo Type</Table.HeaderCell>
                                    <Table.HeaderCell>Discount</Table.HeaderCell>
                                    <Table.HeaderCell>Product</Table.HeaderCell>
                                    <Table.HeaderCell>Quantity</Table.HeaderCell>
                                    <Table.HeaderCell>Shopper Profiles</Table.HeaderCell>
                                    <Table.HeaderCell>Duration</Table.HeaderCell>
                                    <Table.HeaderCell>Start Date</Table.HeaderCell>
                                    <Table.HeaderCell>End Date</Table.HeaderCell>
                                    <Table.HeaderCell>Store</Table.HeaderCell>
                                    <Table.HeaderCell>Confidence Level</Table.HeaderCell>
                                </Table.Row>
                            </Table.Header>
                        </Table>
                    </Segment>
                </Grid.Column>
            </Grid>
        </div>
        )
    }
}
export default PromoSuggestions;