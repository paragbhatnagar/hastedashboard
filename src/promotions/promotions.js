import React, { Component } from 'react';
import {Loader, Dimmer, Button, Dropdown} from 'semantic-ui-react'
import DatePicker from 'react-datepicker';
import Navbar from '../components/navbar';
//import components



// import styling and assets
import '../assets/css/shoppersAnalysis.css';
import '../assets/css/App.css';
import '../assets/css/theme.css';

// import api
import {API_SERVER_URL, AVAILABLE_STORES, PROMOTIONS_SUMMARY} from "../api";


import moment from "moment/moment";
import SidebarMenu from "../components/sidebar";
import PromoPerformance from "./components/promo-performance";
import PromoSuggestions from "./components/promo-suggestions";


class Promotions extends Component{

    constructor(props){

        super(props),
        this.state ={
            is_loading: false,

            storeOptions:[],
            merchant_name:localStorage.getItem("merchant"),

            //selected
            selectedStores:[],
            startDate:  moment().subtract(7, "days"),
            endDate: moment(),
            analysisDisplay: 'promo-performance', 

            promotionsData:[],
        }
        this.handleStartDateChange = this.handleStartDateChange.bind(this);
        this.handleEndDateChange = this.handleEndDateChange.bind(this);
    }

    async componentDidMount(){
        this.setState({is_loading:true});
        const allStores= await this.getStores();
        console.log(await allStores);
        const storeOptions = allStores.map((stores) => {
            return {
                "value": stores.outlet_code,
                "text": stores.outlet_name
            }
        })

        // Default selected stores = all stores
        const selectedStores = storeOptions.map((stores) => {
            return stores.value;
        })

        await this.setState({
            storeOptions: storeOptions,
            selectedStores: selectedStores,
        })
        await this.filter();
        this.setState({is_loading:false});
    }

    async getStores(){
        const body = {
            "merchant_name": this.state.merchant_name
        }
        let allStores = await this.callPostAPI(AVAILABLE_STORES, body);

        //Only move on if there's no error
        if(!allStores.error){
            this.setState({
                storeOptions: allStores,
            })
            return allStores;
        }

        await this.setState({
            storeOptions: allStores,
        })
        return allStores;

    }
    async callPostAPI(api, body){
        let result = {};

        await fetch(API_SERVER_URL + api,{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
            .then((response) => response.json())
            .then((responseJson)=>{
                result = responseJson;
                console.log(responseJson);
            })

        return result;
    }

    async filter(){
        let results = await this.callPostAPI(PROMOTIONS_SUMMARY, {

            merchant_name: this.state.merchant_name,
            start: this.state.startDate,
            end: this.state.endDate,
            outlets: this.state.selectedStores

        });
        //handle result
        if(!results.error) {
            console.log(results);
            this.setState({promotionsData : results});
        }
    }

    handleStartDateChange(date) {
        console.log("Start Date: " + date);
        if(this.state.endDate && date>this.state.endDate){
            date=this.state.endDate;
        }
        this.setState({
            startDate: date
        });
    }

    handleEndDateChange(date) {
        console.log("End Date: " + date);
        if(this.state.startDate && date<this.state.startDate){
            date=this.state.startDate;
        }
        this.setState({
            endDate: date
        });
    }


    render(){
        return (
            <div style={{display: 'flex'}}>
            <Dimmer active={this.state.is_loading}>
                <Loader> Cool things are coming your way... </Loader>
            </Dimmer>
                <SidebarMenu page="promotions" />
                <div className="sales-container">
                    <Navbar 
                        refresh={this.refreshData}
                    />
                    <div className="full-width">
                        <div className="input-container half">
                            <p className="label"> Select Outlet </p>
                            <Dropdown className="store-selector" placeholder='Search for stores' style={{width: '100%'}}
                                      multiple search selection
                                      value={this.state.selectedStores}
                                      onChange={ async (event, data) => {
                                          await this.setState({selectedStores: data.value})
                                      }}
                                      options={this.state.storeOptions} />
                        </div>
                        <div className="input-container">
                            <p className="label">Start Date</p>
                            <DatePicker
                                className="datepicker date-selector" selectsStart
                                selected={this.state.startDate}
                                startDate={this.state.startDate}
                                endDate={this.state.endDate}
                                onChange={this.handleStartDateChange}
                                style={{width: '100%'}}
                                placeholderText="Start Date"
                                locale="en-gb"
                            />
                        </div>
                        <div className="input-container">
                            <p className="label">End Date</p>
                            <DatePicker
                                className="datepicker date-selector" selectsEnd
                                selected={this.state.endDate}
                                startDate={this.state.startDate}
                                endDate={this.state.endDate}
                                onChange={this.handleEndDateChange}
                                placeholderText="End Date"
                                locale="en-gb"
                                style={{width: '100%'}}
                            />
                        </div>
                        </div>
                        <div style={{padding: '400px',paddingTop:'10px', paddingBottom: '0px', width: '100%'}}>
                        <Button
                            className="button" id="data-button" onClick={()=> this.filter() }>Filter Results</Button>
                        </div>
                    <div className="sales-buttons" >
                        <Button
                            fluid
                            className={ "button " + (this.state.analysisDisplay === "promo-performance" ? "active" : "" )}
                            onClick={()=> this.setState({analysisDisplay: "promo-performance"})}>
                            Performance
                        </Button>
                        <Button
                            fluid
                            className={ "button " + (this.state.analysisDisplay === "promo-suggestions" ? "active" : "" )}
                            onClick={()=> this.setState({analysisDisplay: "promo-suggestions"})}>
                            Suggestions
                        </Button>
                    </div>
                            {this.state.analysisDisplay==="promo-performance"? (
                                <PromoPerformance 
                                    data={this.state.promotionsData}
                                />
                                ) : (
                                <PromoSuggestions startDate = {this.state.startDate} endDate = {this.state.endDate} outlets = {this.state.selectedStores}/>
                            )}

                        </div>
                    </div>

        )
    }
}
export default Promotions;