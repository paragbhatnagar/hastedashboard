import React, { Component } from 'react';
import {Button, Card, Grid, Image, Segment} from "semantic-ui-react";
import rabbit from '../../assets/img/gentleman.png';
import Chart from "../../components/chart";
import TreeMap from "react-d3-treemap";
import "react-d3-treemap/dist/react.d3.treemap.css";
import BubbleChart from '@weknow/react-bubble-chart-d3';

// import styling and assets
import '../../assets/css/sales.css';
import '../../assets/css/App.css';
import "react-d3-treemap/dist/react.d3.treemap.css";

class Details extends Component {
    constructor(props) {
        super(props); //userProfile, userProfileDetails

        this.state = {
            profitMargin: 45,
            profitMarginIncrement: "+45",
            avgVisit: 1.4,
            priceSensitivity: "Medium", //Low, High
            chartData: {
                //This is default data
                labels: ['Apr 15', 'Apr 16', 'Apr 17', 'Apr 18', 'Apr 19', 'Apr 20', 'Apr 21'],
                datasets: [
                    {
                        label: 'Visit Frequency ',
                        data: [2, 4, 5, 0, 7, 6, 4],
                    }
                ]
            },
            dataTreeMap: {
                "name": "ALL CATEGORIES",
                "children": [
                    {
                        "name": "FOOD",
                        "children": [

                            {
                                "name": "FRESH PRODUCE",
                                "children": [
                                    {
                                        "name": "FRUITS",
                                        "children": [
                                            {
                                                "name": "Apples",
                                                "value": 100,
                                            },
                                            {"name": "Pears", "value": 80},
                                            {"name": "Strawberries", "value": 80},
                                            {"name": "Peaches", "value": 20}
                                        ]
                                    },
                                    {
                                        "name": "MEAT",
                                        "children": [
                                            {
                                                "name": "MEAT",
                                                "children": [
                                                    {"name": "Pork", "value": 80},
                                                    {"name": "Chicken", "value": 100},
                                                    {"name": "Duck", "value": 50},
                                                    {"name": "Beef", "value": 200},
                                                    {"name": "Seafood", "value": 300}

                                                ]
                                            },
                                            {
                                                "name": "DAIRY",
                                                "children": [
                                                    {"name": "Chocolate Milk", "value": 80},
                                                    {"name": "Skimmed Milk", "value": 110},
                                                    {"name": "Full Cream Milk", "value": 110},
                                                    {"name": "Low Fat Milk", "value": 110}
                                                ]
                                            },
                                            {
                                                "name": "BREAD, CEREAL",
                                                "children": [
                                                    {"name": "KoKo Crunch", "value": 90},
                                                    {"name": "Muesli", "value": 50},
                                                    {"name": "Gardenia", "value": 90},
                                                    {"name": "Milk Bread", "value": 20},
                                                    {"name": "Honey Stars", "value": 60}


                                                ]
                                            }
                                        ]
                                    }]
                            }]
                    },

                    {
                        "name": "NON-FOOD",
                        "children": [
                            {"name": "Others", "value": 150},
                            {"name": "FunctionSequence", "value": 10},
                            {
                                "name": "HOUSEHOLD",
                                "children": [
                                    {"name": "Tote", "value": 100},
                                    {"name": "Laundry Basket", "value": 20},
                                    {"name": "Tupperware", "value": 30},
                                    {"name": "Pans", "value": 40},
                                    {"name": "Baking Needs", "value": 10},
                                    {"name": "Storage Bin", "value": 10},
                                ]
                            }, {
                                "name": "BLADES",
                                "children": [
                                    {"name": "Sharp", "value": 20},
                                    {"name": "Swift", "value": 30},
                                    {"name": "SuperBlade", "value": 50},
                                    {"name": "Gatorade", "value": 10},
                                ]
                            }, {
                                "name": "DIAPERS",
                                "children": [
                                    {"name": "Comfy", "value": 100},
                                    {"name": "Uber", "value": 100},

                                ]
                            }
                        ]
                    },
                    {
                        "name": "DRINKS",
                        "children": [
                            {
                                "name": "Alcohol",
                                "children": [
                                    {"name": "Beer", "value": 50},
                                    {"name": "Wine", "value": 55},
                                    {"name": "Hard Liquor", "value": 120},
                                    {"name": "Spirits", "value": 60},
                                    {"name": "Cocktails", "value": 90}
                                ]
                            }
                        ]
                    }

                ]
            },
            dataTreeMapp: {
                "name": "ALL CATEGORIES",
                "children": [
                    {
                        "name": "FOOD",
                        "children": [

                            {
                                "name": "FRESH PRODUCE",
                                "children": [
                                    {
                                        "name": "FRUITS",
                                        "children": [
                                            {
                                                "name": "Apples",
                                                "value": 10,
                                            },
                                            {"name": "Pears", "value": 8},
                                            {"name": "Strawberries", "value": 8},
                                            {"name": "Peaches", "value": 2}
                                        ]
                                    },
                                    {
                                        "name": "MEAT",
                                        "children": [
                                            {
                                                "name": "MEAT",
                                                "children": [
                                                    {"name": "Pork", "value": 8},
                                                    {"name": "Chicken", "value": 10},
                                                    {"name": "Duck", "value": 50},
                                                    {"name": "Beef", "value": 200},
                                                    {"name": "Seafood", "value": 300}

                                                ]
                                            },
                                            {
                                                "name": "DAIRY",
                                                "children": [
                                                    {"name": "Chocolate Milk", "value": 8},
                                                    {"name": "Skimmed Milk", "value": 11},
                                                    {"name": "Full Cream Milk", "value": 11},
                                                    {"name": "Low Fat Milk", "value": 11}
                                                ]
                                            },
                                            {
                                                "name": "BREAD, CEREAL",
                                                "children": [
                                                    {"name": "KoKo Crunch", "value": 9},
                                                    {"name": "Muesli", "value": 5},
                                                    {"name": "Gardenia", "value": 9},
                                                    {"name": "Milk Bread", "value": 2},
                                                    {"name": "Honey Stars", "value": 6}


                                                ]
                                            }
                                        ]
                                    }]
                            }]
                    },

                    {
                        "name": "NON-FOOD",
                        "children": [
                            {"name": "Others", "value": 15},
                            {"name": "FunctionSequence", "value": 1},
                            {
                                "name": "HOUSEHOLD",
                                "children": [
                                    {"name": "Tote", "value": 10},
                                    {"name": "Laundry Basket", "value": 20},
                                    {"name": "Tupperware", "value": 3},
                                    {"name": "Pans", "value": 4},
                                    {"name": "Baking Needs", "value": 10},
                                    {"name": "Storage Bin", "value": 1},
                                ]
                            }, {
                                "name": "BLADES",
                                "children": [
                                    {"name": "Sharp", "value": 2},
                                    {"name": "Swift", "value": 3},
                                    {"name": "SuperBlade", "value": 5},
                                    {"name": "Gatorade", "value": 10},
                                ]
                            }, {
                                "name": "DIAPERS",
                                "children": [
                                    {"name": "Comfy", "value": 10},
                                    {"name": "Uber", "value": 10},

                                ]
                            }
                        ]
                    },
                    {
                        "name": "DRINKS",
                        "children": [
                            {
                                "name": "Alcohol",
                                "children": [
                                    {"name": "Beer", "value": 5},
                                    {"name": "Wine", "value": 5},
                                    {"name": "Hard Liquor", "value": 20},
                                    {"name": "Spirits", "value": 5},
                                    {"name": "Cocktails", "value": 9}
                                ]
                            }
                        ]
                    }

                ]
            }


        }
    }

    render() {
        const user = this.props.userProfileDetails;
        return(
            <div style={{display: 'flex',backgroundColor: 'white', margin: '15px'}}>
                <Grid style={{flex:1}}>
                    <Grid.Column width={6}>
                        <Card style={{margin: '5%'}}>
                            <Segment.Group>
                                <Segment className="centerTextArea">
                                    <Image src={rabbit} className="imageSize" size='tiny' circular/>
                                </Segment>
                                <Segment className="centerTextArea">
                                    <h2 className="title" style={{flex: 1, fontWeight: 700, marginBottom:'3px'}}>Shopper {user.profile}</h2>
                                    <h4 className="valuecard">{user.desc}</h4>
                                </Segment>
                                <Segment className="centerTextArea">
                                    <Grid>
                                        <Grid.Row columns={2}>
                                            <Grid.Column className="centerTextArea">
                                                <div className="textContainer">
                                                    <h4 className="value" >{user.age}</h4>
                                                </div>
                                                <div className="textContainer">
                                                    <h4 className="valuecard" >AGE</h4>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column className="centerTextArea">
                                                <div className="textContainer">
                                                    <h4 className="value" >{user.gender}</h4>
                                                </div>
                                                <div className="textContainer">
                                                    <h4 className="valuecard" >GENDER</h4>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column className="centerTextArea">
                                                <div className="textContainer">
                                                    <h4 className="value" >{user.avgbasket}</h4>
                                                </div>
                                                <div className="textContainer">
                                                    <h4 className="valuecard" >AVG BASKET</h4>
                                                </div>
                                            </Grid.Column>
                                            <Grid.Column className="centerTextArea">
                                                <div className="textContainer">
                                                    <h4 className="value" >{user.ticketsize}</h4>
                                                </div>
                                                <div className="textContainer">
                                                    <h4 className="valuecard" >AVG TICKET</h4>
                                                </div>
                                            </Grid.Column>
                                        </Grid.Row>
                                    </Grid>
                                    <div style={{marginTop: '3px', width: '75%', alignSelf: 'center'}}>
                                        <Button className="button" id="data-button" onClick= {this.props.close} >CLOSE</Button>
                                    </div>
                                </Segment>
                            </Segment.Group>

                        </Card>
                </Grid.Column>
                <Grid.Column width={10}>
                    <Card style={{topMargin: '5%'}} fluid>
                        <Segment.Group>
                            <Segment>
                                <div>
                                    <Chart
                                        data={this.state.chartData}
                                        title="Visit Frequency"
                                        dcm={2}
                                    />
                                </div>
                            </Segment>
                            <Segment>
                                <Grid>
                                    <Grid.Row columns={3}>
                                        <Grid.Column className="centerTextArea">
                                            <div className="textContainer">
                                                <h4  className="valueHeaders" >AVG PROFIT MARGIN</h4>
                                            </div>
                                            <div className="textContainer" style={{display:'flex',flexDirection: 'column'}}>
                                                <h3 className="title" style={{flex: 1, marginBottom:0}}>{this.state.profitMargin}%</h3>
                                                <h3 className="valueHeaders" style={{color:'green',marginTop:0}}>({this.state.profitMarginIncrement})</h3>
                                            </div>
                                        </Grid.Column>
                                        <Grid.Column className="centerTextArea">
                                            <div className="textContainer">
                                                <h4  className="valueHeaders" ></h4>
                                                <h4  className="valueHeaders" >AVG VISITS</h4>
                                            </div>
                                            <div className="textContainer">
                                                <h3  className="valueHeaders" style={{flex: 1}}>{this.state.avgVisit}x</h3>
                                            </div>
                                        </Grid.Column>
                                        <Grid.Column className="centerTextArea">
                                            <div className="textContainer">
                                                <h4  className="valueHeaders" ></h4>
                                                <h4  className="valueHeaders" >PRICE SENSITIVITY</h4>
                                            </div>
                                            <div className="textContainer">
                                                <h3 className="valueHeaders" style={this.state.priceSensitivity==="Medium"?({color:'green'}):({color:'red'})}>{this.state.priceSensitivity}</h3>
                                            </div>
                                        </Grid.Column>
                                    </Grid.Row>
                                </Grid>
                            </Segment>
                            <Segment>
                                <h3  className="analysisHeader">REVENUE</h3>
                                <TreeMap
                                    height={500}
                                    width={550}
                                    data={this.state.dataTreeMap}
                                    valueUnit={",000 $"}
                                    bgColorRangeLow={"#FFC05D"}
                                    bgColorRangeHigh={"#F26609"} //#F26609,#F27A18,#ED9A25,#FFAF30,#FFC05D,#F26609,#F27A18,#ED9A25
                                    style={{textAlign:'center',justifyContent:'center'}}
                                />
                            </Segment>
                            <Segment>
                                <h3  className="analysisHeader">PROFIT</h3>

                                <TreeMap
                                    height={500}
                                    width={550}
                                    data={this.state.dataTreeMapp}
                                    valueUnit={",000 $"}
                                    bgColorRangeLow={"#FFC05D"}
                                    bgColorRangeHigh={"#F26609"} //#F26609,#F27A18,#ED9A25,#FFAF30,#FFC05D,#F26609,#F27A18,#ED9A25
                                    style={{textAlign:'center',justifyContent:'center'}}
                                />

                            </Segment>
                        </Segment.Group>
                    </Card>
                </Grid.Column>
                </Grid>
            </div>

        )
    }
}
export default Details;