import React, { Component } from 'react';
import {Icon, Button, Dropdown, Card, Grid, Image, Segment} from 'semantic-ui-react'

//import components
import SidebarMenu from '../../components/sidebar';
import Navbar from '../../components/navbar';


// import styling and assets
import '../../assets/css/sales.css';
import '../../assets/css/App.css';
import {API_SERVER_URL, AVAILABLE_STORES, DEMOGRAPHICS_SUMMARY} from "../../api";
import rabbit from '../../assets/img/gentleman.png';
import Details from "./details";


class Behaviour extends Component{
    constructor(props){
        super(props);
        this.state ={
            //initialised
            ageRanges:[
                {profile: 'A', desc:'Healthy and natural', age:'21', gender:'F', avgbasket:5, ticketsize:2},
                {profile: 'B', desc:'Gourmet focus', age:'25', gender:'M', avgbasket:5, ticketsize:2},
                {profile: 'C', desc:'Family feeders ', age:'30', gender:'M', avgbasket:5, ticketsize:2},
                {profile: 'D', desc:'On-the-go', age:'40', gender:'F', avgbasket:5, ticketsize:2},
                {profile: 'E', desc:'Variety seekers', age:'60', gender:'M', avgbasket:5, ticketsize:2},
                {profile: 'F', desc:'Here we go again', age:'80', gender:'M', avgbasket:5, ticketsize:2}
                ],

            //selected
            selectedProfile: null,
            selectedProfileDetails:null,
            screenDisplay: 'shopperTypes', // Details, shopperTypes

            //results

            avgBasketSize: undefined,
            avgTicketSiz: undefined,
            topCategory: undefined,
            topPromotion: undefined,
            frequentVisit: undefined,
        }

    }

//handlers
    async reset(){
        await this.changeProfile(null,null);

        await this.getResults();

    }
    async handleClick(profile,arrayProfile){
        await this.changeProfile(profile,arrayProfile);

        await this.getResults();

    }

    async changeProfile(profile, arrayProfile){
        this.setState({selectedProfile:profile});
        this.setState({selectedProfileDetails:arrayProfile});
        console.log(this.state.selectedProfile)
        return this.state.selectedProfile;
    }

    async getResults(){
        let result = await this.callPostAPI(DEMOGRAPHICS_SUMMARY, {
            ageGroup: this.state.selectedAge
        });
        //handle result
    }


    async callPostAPI(api, body){
        let result = {};
        console.log(""+API_SERVER_URL + api);
        await fetch(API_SERVER_URL + api,{

            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)

        })
            .then((response) => response.json())
            .then((responseJson)=>{
                result = responseJson;
            })

        return result;
    }

    render() {
        return (
            <div>
                <div style={{background:'white',alignSelf:'center',textAlign:'center',margin:'3px',height:'40px'}}><h2 className="title" style={{color:'black',alignSelf:'center'}}>Work in Progress</h2></div>
                {this.state.selectedProfile === null ? (
                    <div style={{display: 'flex'}}>
                        <Grid style={{backgroundColor: 'white', margin: '15px'}}>
                            <Grid.Row columns={3} className="tripleCardsContainer">
                                {this.state.ageRanges.map((user) => (
                                    <Grid.Column style={{margin: '0%'}}>
                                        <Card style={{margin: '5%', color:'black'}}
                                              onClick={() => this.handleClick(user.profile, user)}>
                                            <Segment.Group>
                                                <Segment className="centerTextArea">
                                                    <Image src={rabbit} className="imageSize" size='tiny' circular/>
                                                </Segment>
                                                <Segment className="centerTextArea">
                                                    <h2 className="title" style={{flex: 1, fontWeight: 700, marginBottom:'3px'}}>Shopper {user.profile}</h2>
                                                    <h4 className="valuecard" >{user.desc}</h4>
                                                </Segment>
                                                <Segment className="centerTextArea">
                                                    <Grid>
                                                        <Grid.Row columns={2}>
                                                            <Grid.Column className="centerTextArea">
                                                                <div className="textContainer">
                                                                    <h4  className="value" >{user.age}</h4>
                                                                </div>
                                                                <div className="textContainer">
                                                                    <h4 className="valuecard" >AGE</h4>
                                                                </div>
                                                            </Grid.Column>
                                                            <Grid.Column className="centerTextArea">
                                                                <div className="textContainer">
                                                                    <h4  className="value" >{user.gender}</h4>
                                                                </div>
                                                                <div className="textContainer">
                                                                    <h4  className="valuecard" >GENDER</h4>
                                                                </div>
                                                            </Grid.Column>
                                                            <Grid.Column className="centerTextArea">
                                                                <div className="textContainer">
                                                                    <h4  className="value" >{user.avgbasket}</h4>
                                                                </div>
                                                                <div className="textContainer">
                                                                    <h4  className="valuecard" >AVG BASKET</h4>
                                                                </div>
                                                            </Grid.Column>
                                                            <Grid.Column className="centerTextArea">
                                                                <div className="textContainer">
                                                                    <h4 className="value">{user.ticketsize}</h4>
                                                                </div>
                                                                <div className="textContainer">
                                                                    <h4  className="valuecard" >AVG TICKET</h4>
                                                                </div>
                                                            </Grid.Column>
                                                        </Grid.Row>
                                                    </Grid>
                                                    <div style={{marginTop: '3px', width: '75%', alignSelf: 'center'}}>
                                                        <Button className="button" id="data-button">Details</Button>
                                                    </div>
                                                </Segment>
                                            </Segment.Group>

                                        </Card>
                                    </Grid.Column>
                                ))}
                            </Grid.Row>
                        </Grid>
                    </div>

                ) : (
                    <div>

                        <Details close = {this.reset.bind(this)}
                            userProfile={this.state.selectedProfile}
                                 userProfileDetails={this.state.selectedProfileDetails}/>

                    </div>

                    )
                }
            </div>

        )
    }
}
export default Behaviour;