import React, { Component } from 'react';


import {Segment, Card, Grid, Image, Icon} from "semantic-ui-react";
import {API_SERVER_URL, DEMOGRAPHICS_SUMMARY} from "../../api";
import Ftens from '../../assets/img/FBELOW20.jpg';
import Ftwenties from '../../assets/img/FTWENTIES.jpg';
import Fthirties from '../../assets/img/FTHIRTIES.jpg';
import Fforties from '../../assets/img/FFORTIES.jpg';
import Ffifties from '../../assets/img/FFIFTIES.jpg';
import Fsixties from '../../assets/img/FABOVESIXTIES.jpg';
import Mtens from '../../assets/img/MBELOW20.jpg';
import Mtwenties from '../../assets/img/MTWENTIES.jpg';
import Mthirties from '../../assets/img/MTHIRTIES.jpg';
import Mforties from '../../assets/img/MFORTIES.jpg';
import Mfifties from '../../assets/img/MSIXTIES.jpg';
import Msixties from '../../assets/img/MFIFTIES.jpg';
import '../../assets/css/App.css';
import '../../assets/css/sidebar.css';
import Transactions from "../../dashboard/components/dashboardTransactions";

class Demographics extends Component{
    constructor(props){
        super(props);
        this.state = {
            startDate: this.props.startDate,
            endDate: this.props.endDate,
            selectedoutlets:this.props.selectedoutlets,

            merchant_name: localStorage.getItem("merchant"),
            selectedAge: null, //0:<20s, 1:20-30s,2:30-40s,3:40-50s,4:50s-60s,5:60s>
            gender:null,
            avg_basket_size: null,
            avg_ticket_size: null,
            top_category: null,
            top_promotion:'-',
            frequent_visit: '-',
            results:null
        }

    }

    componentWillReceiveProps(nextProps) {
        this.setState({startDate:nextProps.startDate}),
        this.setState({endDate:nextProps.endDate}),
        this.setState({selectedoutlets:nextProps.selectedoutlets}),
        this.setState({avg_basket_size:null}),
        this.setState({avg_ticket_size:null}),
        this.setState({top_category:null}),
        this.setState({results:null}),
        this.handleClick(null),
        console.log("reset!")
    }

//handlers
    async handleClick(active_age, gender){
        await this.changeAge(active_age);
        await this.changeGender(gender);
        await this.updateResults();
    }

    async changeAge(active_age){
        this.setState({selectedAge:active_age});
        return this.state.selectedAge;
    }
    async changeGender(gender){
        this.setState({gender:gender});
        console.log(gender);
        return this.state.gender;

    }

    async updateResults(){
        if (this.state.results==null) {
            console.log("REFRESHED!")
            const resultArray = await this.getAllResults();
            this.state.results = await resultArray;
        }
        const rawResults = await this.state.results;
        const ageGroup = this.state.selectedAge;
        try{
            const maleFemaleArray = await rawResults.demographic[ageGroup];
            var finalResults = undefined;
            this.state.gender === "female" ? (
                finalResults = maleFemaleArray.female
            ) : (
                finalResults = maleFemaleArray.male
            );
            this.setState({avg_basket_size: finalResults.average_basket_size});
            this.setState({avg_ticket_size: finalResults.average_ticket_size});
            this.setState({top_category: finalResults.top_category});
        }catch(error){
            console.log (error);
        }

    }

    async getAllResults(){
        let results = await this.callPostAPI(DEMOGRAPHICS_SUMMARY, {

            merchant_name: this.state.merchant_name,
            start: this.state.startDate,
            end: this.state.endDate,
            outlets: this.state.selectedoutlets

        });
        //handle result
        console.log("lolol");
        console.log({ merchant_name: this.state.merchant_name,
            start: this.props.startDate,
            end: this.props.endDate,
            outlets: this.props.selectedoutlets})
        this.state.result = await results;
        console.log(this.state.result);
        return results;
    }


    async callPostAPI(api, body){
        let result = {};
        console.log(""+API_SERVER_URL + api);
        await fetch(API_SERVER_URL + api,{

            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
            .then((response) => response.json())
            .then((responseJson)=>{
                result = responseJson;
            })

        return result;
    }

    //hover
    showLabel(text){
        console.log("in");
        return (
            <div>
                <h4 className="value"  >20s</h4>
                <div style={{position: 'fixed',zIndex:10}}>{text}</div>
            </div>

    )
    }


    render(){
        return (
            <Grid style={{backgroundColor:'white',margin:'15px'}}>
                <Grid.Row columns={3} className="tripleCardsContainer" >
                    <Grid.Column style={{margin:0}}>
                        <Card>
                            <div class="ui segments">
                                <div class="ui segment" style={{ fontSize: 0,display:'flex',justifyContent:'flex-start'}}>
                                    <Card className={this.state.selectedAge===0 && this.state.gender==="female"? ("imageSizeActive"): ("imageSize")}
                                          onClick= {() => this.handleClick(0,"female")}>
                                        <Image src={Ftens} fluid />
                                        <div style={{display:'flex', flexDirection:'row'}}>
                                            <h2 className="valueLabel">FEMALE</h2>
                                        </div>
                                    </Card>
                                    <Card  className={this.state.selectedAge===0 && this.state.gender==="male"? ("imageSizeActive"): ("imageSize")}
                                           onClick= {() => this.handleClick(0,"male")}>
                                        <Image  src={Mtens} fluid/>
                                        <div style={{display:'flex', flexDirection:'row'}}>
                                            <h2 className="valueLabel">MALE</h2>
                                        </div>
                                    </Card>
                                </div>
                                <div class="ui segment">

                                    <Grid.Column className="rightTextArea">
                                        <div className="textContainer">
                                            <h2 className="value" style={{color:'grey'}}>age</h2>
                                        </div>
                                        <div className="textContainer">
                                            <h4  className="value" >Below 20</h4>
                                        </div>
                                    </Grid.Column>
                                </div>
                            </div>
                        </Card>
                    </Grid.Column>

                    <Grid.Column style={{margin:0}}>
                        <Card >
                            <div class="ui segments">
                                <div class="ui segment" style={{fontSize: 0,display:'flex',justifyContent:'flex-start'}}>
                                    <Card className={this.state.selectedAge===1 && this.state.gender==="female"? ("imageSizeActive"): ("imageSize")}
                                          onClick= {() => this.handleClick(1,"female")}>
                                        <Image src={Ftwenties} className="imageSize" fluid/>
                                        <h2 className="valueLabel">FEMALE</h2>
                                    </Card>
                                    <Card className={this.state.selectedAge===1 && this.state.gender==="male"?
                                        ("imageSizeActive"): ("imageSize")} onClick= {() => this.handleClick(1,"male")}>
                                        <Image src={Mtwenties} className="imageSize" fluid />
                                        <h2 className="valueLabel">MALE</h2>
                                    </Card>
                                </div>
                                <div class="ui segment">
                                    <Grid.Column className="rightTextArea">
                                        <div className="textContainer">
                                            <h2 className="value" style={{color:'grey'}}  >age</h2>
                                        </div>
                                        <div className="textContainer">
                                            <h4 className="value"  >20s</h4>
                                        </div>
                                    </Grid.Column></div>
                            </div>
                        </Card>
                    </Grid.Column>

                    <Grid.Column style={{margin:0}}>
                        <Card >
                            <div class="ui segments">
                                <div class="ui segment" style={{fontSize: 0,display:'flex',justifyContent:'flex-start'}}>
                                    <Card className={this.state.selectedAge===2 && this.state.gender==="female"? ("imageSizeActive"): ("imageSize")}
                                          onClick= {() => this.handleClick(2,"female")}>
                                        <Image src={Fthirties} className="imageSize" fluid/>
                                        <h2 className="valueLabel">FEMALE</h2>
                                    </Card>
                                    <Card className={this.state.selectedAge===2 && this.state.gender==="male"? ("imageSizeActive"): ("imageSize")}
                                          onClick= {() => this.handleClick(2,"male")}>
                                        <Image src={Mthirties} className="imageSize" fluid/>
                                        <h2 className="valueLabel">MALE</h2>
                                    </Card>
                                </div>
                                <div class="ui segment">
                                    <Grid.Column className="rightTextArea">
                                        <div className="textContainer">
                                            <h2 className="value" style={{color:'grey'}}  >age</h2>
                                        </div>
                                        <div className="textContainer">
                                            <h4 className="value">30s</h4>
                                        </div>
                                    </Grid.Column>
                                </div>
                            </div>
                        </Card>
                    </Grid.Column>
                </Grid.Row>

                {this.state.selectedAge===null?(<div></div>):(
                    <div class="ui segments" style={{marginLeft:'37px',marginRight:'37px',width:'100%'}} >
                        <Icon name="remove" style={{   position: 'absolute', right: 8, top:8}} onClick= {() => this.handleClick(null)}/>
                        <div style={{display:'flex', justifyContent: 'space-between',marginTop:15, marginBottom:10}} >
                            <div className="centerTextArea">
                                <div className="textContainer">
                                    <h3 className="title" >AVG BASKET SIZE</h3>
                                </div>
                                <div className="textContainer">
                                    <h2 className="value" >{this.state.avg_basket_size}</h2>
                                </div>
                            </div>

                            <div className="centerTextArea">
                                <div className="textContainer">
                                    <h3 className="title" >AVG TICKET SIZE</h3>
                                </div>
                                <div className="textContainer">
                                    <h2 className="value" >{this.state.avg_ticket_size}</h2>
                                </div>
                            </div>

                            <div className="centerTextArea">
                                <div className="textContainer">
                                    <h3 className="title">TOP CATEGORY</h3>
                                </div>
                                <div className="textContainer">
                                    <h2 className="value" >{this.state.top_category}</h2>
                                </div>
                            </div>

                            <div className="centerTextArea">
                                <div className="textContainer">
                                    <h3 className="title">TOP PROMOTION</h3>
                                </div>
                                <div className="textContainer">
                                    <h2 className="value" >{this.state.top_promotion}</h2>
                                </div>

                            </div>

                            <div className="centerTextArea">
                                <div className="textContainer" style={{paddingLeft:'10px', paddingRight:'10px'}}>
                                    <h3 className="title" >FREQUENT VISIT</h3>
                                </div>
                                <div className="textContainer">
                                    <h2 className="value" style={{paddingLeft:'10px'}}>{this.state.frequent_visit}</h2>
                                </div>
                            </div>

                        </div>
                    </div>
                )}

                <Grid.Row columns={3} className="tripleCardsContainer" >
                    <Grid.Column style={{margin:0}}>
                        <Card >
                            <div class="ui segments">
                                    <div class="ui segment" style={{fontSize: 0,display:'flex',justifyContent:'flex-start'}}>
                                        <Card className={this.state.selectedAge===3 && this.state.gender==="female"? ("imageSizeActive"): ("imageSize")}
                                              onClick= {() => this.handleClick(3,"female")}>
                                             <Image src={Fforties} className="imageSize" fluid/>
                                            <h2 className="valueLabel">FEMALE</h2>
                                        </Card>
                                        <Card className={this.state.selectedAge===3 && this.state.gender==="male"? ("imageSizeActive"): ("imageSize")}
                                              onClick= {() => this.handleClick(3,"male")}>
                                            <Image src={Mforties} className="imageSize" fluid/>
                                            <h2 className="valueLabel">MALE</h2>
                                        </Card>
                                    </div>
                                <div class="ui segment" style={{alignItems:'flex-end'}}>
                                    <Grid.Column className="rightTextArea">
                                        <div className="textContainer">
                                            <h2 className="value" style={{color:'grey'}}>age</h2>
                                        </div>
                                        <div className="textContainer">
                                            <h4 className="value" >40s</h4>
                                        </div>
                                    </Grid.Column>
                                </div>
                            </div>
                        </Card>
                    </Grid.Column>

                    <Grid.Column style={{margin:0}}>
                        <Card >
                            <div class="ui segments">
                                <div class="ui segment" style={{fontSize: 0,display:'flex',justifyContent:'flex-start'}}>
                                    <Card className={this.state.selectedAge===4 && this.state.gender==="female"? ("imageSizeActive"): ("imageSize")}
                                        onClick= {() => this.handleClick(4,"female")}>
                                        <Image src={Ffifties} className="imageSize" fluid/>
                                        <h2 className="valueLabel">FEMALE</h2>
                                    </Card>
                                    <Card className={this.state.selectedAge===4 && this.state.gender==="male"? ("imageSizeActive"): ("imageSize")}
                                        onClick= {() => this.handleClick(4,"male")}>
                                        <Image src={Mfifties} className="imageSize" fluid/>
                                        <h2 className="valueLabel">MALE</h2>
                                    </Card>
                                </div>
                                <div class="ui segment">
                                    <Grid.Column className="rightTextArea">
                                        <div className="textContainer">
                                            <h2 className="value" style={{color:'grey'}}  >age</h2>

                                        </div>
                                        <div className="textContainer">
                                            <h4 className="value">50s</h4>

                                        </div>
                                    </Grid.Column>
                                </div>
                            </div>
                        </Card>
                    </Grid.Column>

                    <Grid.Column style={{margin:0}}>
                        <Card >
                            <div class="ui segments">
                                <div class="ui segment" style={{ fontSize: 0,display:'flex',justifyContent:'flex-start'}}>
                                    <Card className={this.state.selectedAge===5 && this.state.gender==="female"? ("imageSizeActive"): ("imageSize")}
                                          onClick= {() => this.handleClick(5,"female")}>
                                        <Image src={Fsixties} className="imageSize" fluid />
                                        <h2 className="valueLabel">FEMALE</h2>
                                    </Card>
                                    <Card className={this.state.selectedAge===5 && this.state.gender==="male"? ("imageSizeActive"): ("imageSize")}
                                          onClick= {() => this.handleClick(5,"male")}>
                                        <Image src={Msixties} className="imageSize" fluid/>
                                        <h2 className="valueLabel">MALE</h2>
                                    </Card>
                                </div>
                                <div class="ui segment">
                                    <Grid.Column className="rightTextArea">
                                        <div className="textContainer">
                                            <h2 className="value" style={{color:'grey'}}  >age</h2>

                                        </div>
                                        <div className="textContainer">
                                            <h4 className="value">Above 60</h4>

                                        </div>
                                    </Grid.Column>
                                </div>
                            </div>
                        </Card>
                    </Grid.Column>
                </Grid.Row>

            </Grid>


        )
    }
}
export default Demographics;