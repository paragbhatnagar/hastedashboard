import React, { Component } from 'react';
import {Icon, Button, Dropdown, Dimmer, Loader} from 'semantic-ui-react'
import DatePicker from 'react-datepicker';
//import components



// import styling and assets
import '../assets/css/shoppersAnalysis.css';
import '../assets/css/App.css';
import '../assets/css/theme.css';

// import api
import {API_SERVER_URL, AVAILABLE_STORES} from "../api";


import moment from "moment/moment";
import SidebarMenu from "../components/sidebar";
import Navbar from '../components/navbar';
import Behaviour from "./components/behaviour";
import Demographics from "./components/demographics";




class ShoppersAnalysis extends Component{

    constructor(props){

        super(props),
        this.state ={
            is_loading: false,
            // to initialize
            storeOptions:[],
            merchant_name:localStorage.getItem("merchant"),
            //selected
            selectedStores:[],
            startDate: moment().subtract(7, "days"),
            endDate: moment(),
            analysisDisplay: 'demographic', // Demographic,behavioural
        }
        this.handleStartDateChange = this.handleStartDateChange.bind(this);
        this.handleEndDateChange = this.handleEndDateChange.bind(this);
    }

    async componentDidMount(){
        this.setState({is_loading:true});
        const allStores = await this.getStores();
        console.log(await allStores);
        const storeOptions = allStores.map((stores) => {
            return {
                "value": stores.outlet_code,
                "text": stores.outlet_name
            }
        })

        // Default selected stores = all stores
        const selectedStores = storeOptions.map((stores) => {
            return stores.value;
        })

        this.setState({
            storeOptions: storeOptions,
            selectedStores: selectedStores,
            is_loading: false
        })
    }

    async getStores(){
        const body = {
            "merchant_name": this.state.merchant_name
        }
        let allStores = await this.callPostAPI(AVAILABLE_STORES, body);

        //Only move on if there's no error
        if(!allStores.error){
            this.setState({
                storeOptions: allStores,
            })
            return allStores;
        }

        await this.setState({
            storeOptions: allStores,
        })
        console.log(allStores);
        return allStores;

    }
    async callPostAPI(api, body){
        let result = {};

        await fetch(API_SERVER_URL + api,{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
            .then((response) => response.json())
            .then((responseJson)=>{
                result = responseJson;
                console.log(responseJson);
            })

        return result;
    }



    // ##############################
    // // // Handlers
    // ##############################
    handleStartDateChange(date) {
        console.log("Start Date: " + date);
        if(this.state.endDate && date>this.state.endDate){
            date=this.state.endDate;
        }
        this.setState({
            startDate: date
        });
    }

    handleEndDateChange(date) {
        console.log("End Date: " + date);
        if(this.state.startDate && date<this.state.startDate){
            date=this.state.startDate;
        }
        this.setState({
            endDate: date
        });
    }


    render(){
        return (
            <div style={{display: 'flex'}}>
            <Dimmer active={this.state.is_loading}>
                <Loader> Cool things are coming your way... </Loader>
            </Dimmer>
                <SidebarMenu page="shoppers-analysis" />
                <div className="sales-container">
                    <Navbar 
                        refresh={this.refreshData}
                    />
                    <div className="full-width">
                        <div className="input-container half">
                            <p className="label"> Select Outlet </p>
                            <Dropdown className="store-selector" placeholder='Search for stores' style={{width: '100%'}}
                                  multiple search selection
                                  value={this.state.selectedStores}
                                  onChange={ async (event, data) => {
                                      await this.setState({selectedStores: data.value})
                                  }}
                                  options={this.state.storeOptions} />
                        </div>
                        <div className="input-container">
                            <p className="label">Start Date</p>
                            <DatePicker
                                className="datepicker date-selector" selectsStart
                                selected={this.state.startDate}
                                startDate={this.state.startDate}
                                endDate={this.state.endDate}
                                onChange={this.handleStartDateChange}
                                style={{width: '100%'}}
                                placeholderText="Start Date"
                                locale="en-gb"
                            />
                        </div>
                        <div className="input-container">
                            <p className="label">End Date</p>
                            <DatePicker
                                className="datepicker date-selector" selectsEnd
                                selected={this.state.endDate}
                                startDate={this.state.startDate}
                                endDate={this.state.endDate}
                                onChange={this.handleEndDateChange}
                                placeholderText="End Date"
                                locale="en-gb"
                                style={{width: '100%'}}
                            />
                        </div>
                        </div>

                    <div className="sales-buttons" >
                        <Button
                            fluid
                            className={ "button " + (this.state.analysisDisplay === "demographic" ? "active" : "" )}
                            onClick={()=> this.setState({analysisDisplay: "demographic"})}>
                            Demographic
                        </Button>
                        <Button
                            fluid
                            className={ "button " + (this.state.analysisDisplay === "behavioural" ? "active" : "" )}
                            onClick={()=> this.setState({analysisDisplay: "behavioural"})}>
                            Behavioural
                        </Button>
                    </div>

                            {this.state.analysisDisplay==="demographic"? (
                                <Demographics startDate = {this.state.startDate} endDate = {this.state.endDate} selectedoutlets = {this.state.selectedStores}/>
                                ) : (
                                    <Behaviour startDate = {this.state.startDate} endDate = {this.state.endDate} selectedoutlets = {this.state.selectedStores}/>
                            )}

                        </div>
                    </div>

        )
    }
}
export default ShoppersAnalysis;