// export const API_SERVER_URL = 'http://192.168.1.4:3001'; // backend server URL
// export const API_SERVER_URL = 'http://127.0.0.1:3002'; // backend server URL
export const API_SERVER_URL = 'https://hastelive.com/portalbackend'; // backend server URL

// GET REQUESTS
export const TOTAL_AVAILABLE_STORES_SALES_THIS_WEEK       = '/transactionResource/total_AVAILABLE_STORES_sales';
export const PERCENT_AVAILABLE_STORES_SALES_THIS_WEEK     = '/transactionResource/all_store_percentage';

export const AVG_ALL_STORE_SHOPPERS_THIS_WEEK       = '/customerResource/AvgShopperFrequencyAllStore';
export const PERCENT_ALL_STORE_SHOPPERS_THIS_WEEK   = '/customerResource/percentageAvgShopperFrequencyAllStore';
export const PERCENT_ALL_STORE_U_SHOPPERS_THIS_WEEK = '/customerResource/percentageAllNoOfShopper';

// POST REQUESTS
export const LOGIN                                  = '/accountResource/merchantlogin';
export const TOTAL_STORE_SALES_THIS_WEEK            = '/transactionResource/total_store_sales'; // takes outlet_id
export const PERCENT_STORE_SALES_THIS_WEEK          = '/transactionResource/store_percentage'; // takes outlet_id
export const ANALYSIS_OVERALL_SALES                 = '/transactionResource/AnalysisOverallSales'; // takes analysis params

export const PERCENT_STORE_U_SHOPPERS_THIS_WEEK     = '/customerResource/percentageFilteredNoOfShopper'; // takes outlet_id
export const AVG_STORE_SHOPPERS_THIS_WEEK           = '/customerResource/AvgShopperFrequencyFilteredStore'; // takes outlet_id
export const PERCENT_STORE_SHOPPERS_THIS_WEEK       = '/customerResource/percentageFilteredNoOfShopper'; // takes outlet_id

export const ANALYSIS_NUM_PRODS                     = '/transactionResource/TotalProductSold'; // takes analysis params

export const REPORT_ALL                             = '/reportResource/generateReport';

export const DEMOGRAPHICS_SUMMARY                   = '/shopperResource/shopperDemographic';

export const PRICEELASTICITYOFDEMAND_PROMO           = '/priceElasticityResource/elasticityDemand_Promo';

export const AFFINITYSIMPLE                          = '/productAffinityResource/affinitySimple';

export const AFFINITITEMTOCATEGORYSIMPLE             = '/productAffinityResource/affinityItemToCategorySimple';

export const AFFINITYITEMTOCATEGORYSIMPLE            = '/productAffinityResource/affinityCategoriesSimple';

// Filters
export const AVAILABLE_STORES                       = '/filterResource/getAllOutlets';
export const CATEGORIES                             = '/filterResource/getAllCategories';
export const PRODUCTS                               = '/filterResource/getAllProducts';
export const SUBCATEGORIES                          = '/filterResource/getAllSubCategories';

/**
 * Dashboard Page API Endpoints
 */
export const DASHBOARD_SUMMARY                      = '/dashboardResource/dashboardSummary';
export const SALES_TIME_GRAPH                       = '/dashboardResource/salesTimeGraph'; //takes group_option and outlets
export const PROMOTIONS                             = '/dashboardResource/promotionsTable';
export const CATEGORES_SALES                        = '/dashboardResource/categoriesTable';
export const LATEST_TRX                             = '/dashboardResource/transactionsTable';

/**
 * Sales Page API Endpoints
 */
 export const PAST_WEEK_SALES_SUMMARY               = '/salesResource/salesSummaryForWeek';
 export const SALES_ANALYSIS                        = '/salesResource/salesAnalysis';
 export const ANALYSIS_SALES_TIME_GRAPH_DATA        = '/salesResource/analysisSales-TimeGraph';
 export const ANALYSIS_AVG_BASKET_GRAPH_DATA        = '/salesResource/analysisAverage-BasketGraph';
 export const ANALYSIS_AVG_TICKET_GRAPH_DATA        = '/salesResource/analysisAverage-TicketGraph';

/**
 * PROMO Page API Endpoints
 */

export const PROMOTIONS_SUMMARY =  '/promotionResource/promotions';