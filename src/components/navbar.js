import React, { Component } from 'react';
import FaRefresh from 'react-icons/lib/fa/refresh';
import { Button , Breadcrumb, Icon} from 'semantic-ui-react';

//import styling and assets
import '../assets/css/App.css';
import '../assets/css/navbar.css';
import '../assets/css/theme.css';
import {Redirect} from 'react-router-dom'
import ShoppersAnalysis from '../shoppers/shoppersAnalysis'
import {parse} from 'json2csv'

class SidebarMenu extends Component {
    constructor(props){
        super(props);
        this.state = {
            loggedIn: true,
            merchant_name: localStorage.getItem("merchant"),
            userInfoHidden: true,
        }
    }

    logout = () => {
        console.log("loggin out");
        localStorage.removeItem("session");
        this.setState({loggedIn: false});
    }

    async componentWillMount() {
        var session = localStorage.getItem("session");
        if(session === null ) {
            this.setState({loggedIn: false});
        }
        
    }

    render() {
        if(!this.state.loggedIn) {
            return <Redirect to='/'/>
        }

        return (
            <div className="navbar-menu">
                <Button className="title" onClick={() => {
                    this.setState({userInfoHidden: !this.state.userInfoHidden})
                }}>
                    <Icon size="large" name="user circle"/>
                    <h5>
                        {this.state.merchant_name}
                    </h5>
                </Button>
                <div className={this.state.userInfoHidden ? "user-info hidden" : "user-info"}>
                    <div className="refresh">
                        <Button 
                            icon size="small"
                            className="chart-icon button"
                            color="red"
                            onClick={this.logout}>
                            Logout
                        </Button>
                        <div style={{width: '1px', margin: '5px'}}></div>
                        <Button 
                            icon size="small"
                            className="chart-icon button"
                            onClick={this.props.refresh}>
                            <FaRefresh/>
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}

export default SidebarMenu;
