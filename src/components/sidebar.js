import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Container } from 'semantic-ui-react';

//import styling and assets
import logo from '../assets/img/haste-logo.png';
import '../assets/css/App.css';
import '../assets/css/sidebar.css';

class SidebarMenu extends Component {
    render() {
        return (
            <Container className="sidebar-menu">
            <div className="sidebar-header">
                <img className="sidebar-header-img" src={logo} alt=""/>
                <h3 className="sidebar-header-title">Haste</h3>
            </div>
            <ul className="sidebar-list">
                <li className={ "sidebar-list-item " + (this.props.page === "dashboard" ? "active" : "") }>
                    <NavLink to="/dashboard" className="sidebar-list-item-link">
                        <span className="sidebar-list-item-icon"><i className="fas fa-chart-bar"></i></span>
                        <h4>Dashboard</h4>
                    </NavLink>
                </li>
                <li className={ "sidebar-list-item " + (this.props.page === "sales" ? "active" : "") }>
                    <NavLink to="/sales" className="sidebar-list-item-link">
                        <span className="sidebar-list-item-icon"><i className="fas fa-money-bill-alt"></i></span>
                        <h4>Sales</h4>
                    </NavLink>
                </li>
                {/* <li className={ "sidebar-list-item " + (this.props.page === "shoppers-analysis" ? "active" : "") }>
                    <NavLink to="/shoppers-analysis" className="sidebar-list-item-link">
                        <span className="sidebar-list-item-icon"><i className="fas fa-shopping-cart"></i></span>
                        <h4>Shoppers</h4>
                    </NavLink>
                </li> */}
                <li className={ "sidebar-list-item " + (this.props.page === "products" ? "active" : "") }>
                    <NavLink to="/products" className="sidebar-list-item-link">
                        <span className="sidebar-list-item-icon"><i className="fas fa-box"></i></span>
                        <h4>Products</h4>
                    </NavLink>
                </li>  
                <li className={ "sidebar-list-item " + (this.props.page === "promotions" ? "active" : "") }>
                    <NavLink to="/promotions" className="sidebar-list-item-link">
                        <span className="sidebar-list-item-icon"><i className="fas fa-dollar-sign"></i></span>
                        <h4>Promotions</h4>
                    </NavLink>
                </li> 
                <li className={ "sidebar-list-item " + (this.props.page === "shoppers-analysis" ? "active" : "") }>
                    <NavLink to="/shoppers-analysis" className="sidebar-list-item-link">
                        <span className="sidebar-list-item-icon"><i className="fas fa-shopping-cart"></i></span>
                        <h4>Shoppers</h4>
                    </NavLink>
                </li>
            </ul>
            </Container>
        );
    }
}

export default SidebarMenu;
