// ##############################
// // // Chart Component 
// #############################

/*
    To use Chart Component, pass in these props:
        .Required:
            1. `title` which is the title of the graph

        .Optional (not implemented yet):
            1. `type`: type of the graph being displayed. Not implemented yet
            2. `data` in the following format
                {
                    labels  : Array of labels to use in the x-axis
                    datasets: [
                        {
                            label   : String which indicates the name of the data in y-axis
                            data    : Array of number that corresponds to the labels
                        }
                    ]
                    
                    *Number of data in data arrays should ideally match the number of data in labels array
                }
                If there is no data being passed in, use default data.

    Currently, Chart component will display a line graph with default values 0 for labels Mon-Sun. Data in
    the y-axis are in 2 decimal places.
*/

import React, {Component} from 'react';
import PropTypes from "prop-types";
import {Bar, Line, Pie, Doughnut} from 'react-chartjs-2'
import FaRefresh from 'react-icons/lib/fa/refresh'
import { Button , Divider} from 'semantic-ui-react'
import '../assets/css/App.css';
import '../assets/css/chart.css';

class Chart extends Component {

    render(){
        return(
            <div className="chart-wrapper">
                <div className="chart-header">
                    {/*Use title from props*/}
                    <div style={{display: 'inline'}}>
                        <h3 className="chart-title">{this.props.title}</h3>
                    </div>
                </div>
                
                <div className="chart-graph">
                    {/*Display Line Graph*/}
                    <Line
                        redraw={true}
                        data={this.props.data}
                        height={this.props.height || 100}
                        options={{
                            maintainAspectRatio: true,
                            title:{
                                //Hide title because title can only be in the middle
                                display:false,
                            },
                            legend:{
                                //Hides legend
                                display:false,
                            },
                            scales: {
                                yAxes: [{
                                    ticks: {
                                        //Make sure no negative data
                                        beginAtZero: true,
                                        userCallback: (label, index, labels) => {
                                            // format label to 2 decimal place
                                            return Number(label).toFixed(this.props.dcm);
                                        },
                                    }
                                }],
                            },
                        }}
                    />
                </div>
            </div>
        )
    }
}

Chart.defaultProps = {
    type: "Line",
};

Chart.propTypes = {
    //Required Props
    title: PropTypes.string.isRequired,
    data: PropTypes.object.isRequired,
    dcm: PropTypes.number.isRequired
    //Optional Props
    
    //type: PropTypes.string,
}

export default Chart