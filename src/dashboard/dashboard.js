import React, { Component } from 'react';
import { Dropdown, Dimmer, Loader } from 'semantic-ui-react';

//import components 
import SidebarMenu from '../components/sidebar';
import Navbar from '../components/navbar';
import Chart from '../components/chart';
import Promotions from './components/dashboardPromotions'
import Categories from './components/dashboardCategories';
import Transactions from './components/dashboardTransactions';

// import styling and assets
import '../assets/css/theme.css';
import '../assets/css/dashboard.css';
import '../assets/css/App.css';
// import registerServiceWorker from '../registerServiceWorker'

// import api
import {
    AVAILABLE_STORES,
    API_SERVER_URL,
    DASHBOARD_SUMMARY,
    SALES_TIME_GRAPH,
    PROMOTIONS,
    CATEGORES_SALES,
    CATEGORIES,
    LATEST_TRX
} from '../api';

const months = ["nil", "Jan", "Feb", "Mar", "Apr", "May", "Jun",
                "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]

class Dashboard extends Component {
    constructor(props){
        super(props);
        this.state = {
            is_loading: false,
            merchant_name: localStorage.getItem("merchant"),
            session: localStorage.getItem("session"),
            durationOptions:[
                {
                    value:"week",
                    text:"This Week"
                },
                {
                    value:"month",
                    text:"This Month"
                },
                {
                    value:"3months",
                    text:"3 Months"
                }
            ],
            selectedDuration: "week",
            storeOptions: [],
            selectedStores: [],

            avgTicketSize: 0,
            avgBasketSize: 0,
            noOfTransactions: 0,
            uniqueCustomers: 0, 
            chartData:{
                labels: ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                datasets:[
                    {
                        label:'Sales Volume',
                        data:[0,0,0,0,0,0,0],
                    }
                ]
            },
            promotionsData: [],
            categorySalesData: [],
            latestTrxData: []
        }
        this.setState({
            merchant_name: localStorage.getItem("merchant")
        })
    }

    async componentDidMount(){
        this.setState({is_loading:true});
        console.log("session: ", this.state.session)
        console.log("merchant: ", this.state.merchant_name)
        if(this.state.session == null || this.state.merchant_name == null) {
            // return <Redirect to=''/>
            window.location.href = '/';
        }

        const allStores = await this.getAllStores();
        
        const storeOptions = allStores.map((stores) => {
            return {
                "value": stores.outlet_code,
                "text": stores.outlet_name
            }
        })
        
        // Default selected stores = all stores
        const selectedStores = storeOptions.map((stores) => {
            return stores.value;
        })

        this.setState({
            storeOptions: storeOptions,
            selectedStores: selectedStores,
        })
        await this.loadDashboard(this.state.selectedDuration, this.state.selectedStores)
        this.setState({is_loading:false});
    }

    async getAllStores(){
        const body = {
            "merchant_name": this.state.merchant_name
        }
        let allStores = await this.callPostAPI(AVAILABLE_STORES, body);
        
        //Only move on if there's no error
        if(!allStores.error){
            this.setState({
                storeOptions: allStores,
            })
            return allStores;
        } 

        this.setState({
            storeOptions: allStores,
        })
        return allStores;
    }

    async loadDashboard(selectedDuration, selectedStores){
        await this.getDashboardSummary(selectedDuration, selectedStores);
        await this.getSalesChartData(selectedDuration, selectedStores);
        await this.getPromotions(selectedDuration, selectedStores);
        await this.getCategorySales(selectedDuration, selectedStores);
        await this.getLatestTrx(selectedDuration, selectedStores);
    }

    async getDashboardSummary(selectedDuration, selectedStores) {
        const body = this.prepareFilterBody(selectedDuration, selectedStores);  

        let dashboardSummaryResult = await this.callPostAPI(DASHBOARD_SUMMARY, body);
        console.log(selectedStores)
        console.log(dashboardSummaryResult)
        if(!dashboardSummaryResult.error){
            this.setState({
                avgTicketSize: dashboardSummaryResult.average_ticket_size,
                avgBasketSize: dashboardSummaryResult.average_basket_size,
                noOfTransactions: dashboardSummaryResult.transactions,
                uniqueCustomers: dashboardSummaryResult.unique_customers, 
            })
        }
    }

    async getSalesChartData(selectedDuration, selectedStores) {
        const body = this.prepareFilterBody(selectedDuration, selectedStores);
        let salesChartResult = await this.callPostAPI(SALES_TIME_GRAPH ,body);
        if(!salesChartResult.error){
             
            let labels = salesChartResult.labels;
            let datasets = salesChartResult.datasets;
            let formatLabels = [];

            if (selectedDuration === 'week' || selectedDuration === 'month'){
                labels.forEach(function(e) {
                    formatLabels.push(new Date(e).toDateString());
                }, this);
                salesChartResult = {
                    "labels": formatLabels,
                    "datasets": datasets
                }
            }
            else if (selectedDuration === '3months'){
                labels.forEach(function(e) {
                    formatLabels.push(months[e] + " " + (new Date()).getFullYear())
                }, this);
                salesChartResult = {
                    "labels": formatLabels,
                    "datasets": datasets
                }
            }

            this.setState({
                chartData: salesChartResult
            })
        }
    }

    async getPromotions(selectedDuration, selectedStores) {
        const body = this.prepareFilterBody(selectedDuration, selectedStores);
        
        let promotions = await this.callPostAPI(PROMOTIONS, body);
        if(!promotions.error){
            this.setState({
                promotionsData: promotions
            })
            console.log(this.state.promotionsData)
        } 
    }

    async getCategorySales(selectedDuration, selectedStores) {
        const body = this.prepareFilterBody(selectedDuration, selectedStores);
        
        let categorySalesData = await this.callPostAPI(CATEGORES_SALES, body);
        if(!categorySalesData.error){
            this.setState({
                categorySalesData: categorySalesData
            })
        }
    }

    async getLatestTrx(selectedDuration, selectedStores) {
        const body = this.prepareFilterBody(selectedDuration, selectedStores);
        
        let latestTrx = await this.callPostAPI(LATEST_TRX, body);
        if(!latestTrx.error){
            this.setState({
                latestTrxData: latestTrx
            })
        } 
    }
    
    prepareFilterBody(selectedDuration, selectedStores){        
        const body = {
            filter: selectedDuration,
            outlets: selectedStores,
            merchant_name: this.state.merchant_name
        }
        return body;
    }

    async callGetAPI(api){
        let result = {};
        await fetch(API_SERVER_URL + api,{
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
        .then((response) => response.json())
        .then((responseJson)=>{
            result = responseJson;
        })

        return result;
    }

    async callPostAPI(api, body){
        let result = {};
        
        await fetch(API_SERVER_URL + api,{
            method: 'POST', 
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
        .then((response) => response.json())
        .then((responseJson)=>{
            result = responseJson;
        })
        .catch(e =>{console.log(e)})
        return result;
    }

    refreshData= () => {
        // Check if there are stores selected
        let selectedStores = this.state.selectedStores;
        let selectedDuration = this.state.selectedDuration;
        
        // let storeOptions = this.state.storeOptions;

        // if(selectedStores.length===0){
        //     selectedStores = storeOptions;
        // } else {
        //     let result = storeOptions.filter((curStore, i)=>{
        //         return selectedStores.indexOf(curStore.value)!== -1;
        //     });
        //     selectedStores = result;
        // }
        // if(selectedDuration === null) {
        //     selectedDuration = "week"
        // }
        this.loadDashboard(selectedDuration, selectedStores);
    }

    getDateRange = () => {
        const today = new Date();
        const date = today.getDate()
        const month = today.getMonth();
        const day = today.getDay();
        
        const startDate = new Date();
        const selectedDuration = this.state.selectedDuration;
        switch(selectedDuration){
            case "month":
                startDate.setDate(1);
                break;
            case "3months":
                startDate.setMonth(month-3);
                startDate.setDate(1);
                break;
            default:
                startDate.setDate(startDate.getDate() - startDate.getDay() + 1);
                break;
        }
        return(<h3>Time Duration: {startDate.toDateString()} - {today.toDateString()}</h3>)
    }

    render() {
        return (
            <div style={{display: 'flex', width: '100%'}}>
            <Dimmer active={this.state.is_loading}>
                <Loader> Cool things are coming your way... </Loader>
            </Dimmer>
                <SidebarMenu page="dashboard"/>
                <div className="dashboard-container">
                    <Navbar refresh={
                        this.refreshData.bind(this)
                    }
                />
                <div className="dashboard-input-header">
                    <div className="duration-selector-container">
                        <Dropdown className="duration-selector" defaultValue={'week'} 
                            selection 
                            onChange={
                                async (event, data) => {
                                    await this.setState({
                                        selectedDuration: data.value
                                    })
                                    this.refreshData()
                                }
                            }
                            options={
                                this.state.durationOptions
                            } 
                        />
                    </div>
                    <div className="store-selector-container">                            
                        <Dropdown className="store-selector" placeholder='Search for stores' 
                            multiple search selection 
                            value={this.state.selectedStores}
                            onChange={ 
                                async (event, data) => {
                                    await this.setState({
                                        selectedStores: data.value
                                    })
                                    this.refreshData()
                                }
                            }
                            options={
                                this.state.storeOptions
                            } 
                        />
                    </div>
                </div>

                <div className="duration-display">
                    {
                        this.getDateRange()
                    }
                </div>
                    
                <div className="dash-stats">
                    <div className="card" id="avg-ticket-size">
                        <h3 className="title">Average Ticket Size</h3>
                        <h2 className="value">${Number(this.state.avgTicketSize).toFixed(2)}</h2>
                    </div>
                    <div className="card" id="avg-basket-size">
                        <h3 className="title">Average Basket Size</h3>
                        <h2 className="value">{this.state.avgBasketSize}</h2>
                    </div>
                    <div className="card" id="no-of-transactions">
                        <h3 className="title">No. of Transactions</h3>
                        <h2 className="value">{this.state.noOfTransactions}</h2>
                    </div>
                    <div className="card" id="unique-customers">
                        <h3 className="title">Unique Customers</h3>
                        <h2 className="value">{this.state.uniqueCustomers}</h2>
                    </div>
                </div>
                <div>
                    <Chart 
                        data={this.state.chartData}
                        title="Sales-Time Graph"
                        dcm={2}
                    />
                </div>
                    
                <div style={{
                    display: 'flex',
                    flex: 1,
                    padding: '10px',
                    backgroundColor: 'transparent',

                }}>
                    <div style={{ flex: 1, padding: '10px' }}>
                        <Promotions
                            data={this.state.promotionsData}
                        />
                    </div>
                    <div style={{ flex: 1, padding: '10px' }}>
                        <Categories
                            data={this.state.categorySalesData}
                        />
                    </div>
                </div>

                <div style={{
                    padding: '20px',
                    backgroundColor: 'transparent',

                    }}>
                    <Transactions
                        data={this.state.latestTrxData}
                    />
                </div>
            </div>
        </div>   
        );
    }
}

export default Dashboard;
