import React, {Component} from 'react';
import PropTypes from "prop-types";
import FaRefresh from 'react-icons/lib/fa/refresh';
import { Button, Divider, Table, Menu, Icon } from 'semantic-ui-react';
import { Pagination } from 'semantic-ui-react'

import '../../assets/css/chart.css';

class Transactions extends Component {
    constructor(props){
        super(props);
        this.state={
            curActivePage:1
        }
    }

    handlePageChange = (e, { activePage }) => this.setState({ curActivePage: activePage })

    render(){
        var noRows = 0;
        return(
            <div className="table-wrapper">
                <div className="chart-header">
                    {/*Use title from props*/}
                    <div style={{display: 'inline'}}>
                        <h3 className="chart-title">Latest Transactions</h3>
                    </div>
                </div>

                <Table celled className="table">
                    <Table.Header>
                    <Table.Row>
                        {/* <Table.HeaderCell>Receipt Number</Table.HeaderCell> */}
                        <Table.HeaderCell>DateTime of Purchase</Table.HeaderCell>
                        <Table.HeaderCell>No. Items Purchased</Table.HeaderCell>
                        <Table.HeaderCell>Age</Table.HeaderCell>
                        <Table.HeaderCell>Gender</Table.HeaderCell>
                        <Table.HeaderCell>Store Name</Table.HeaderCell>
                        <Table.HeaderCell>Total Receipt Amount</Table.HeaderCell>
                    </Table.Row>
                    </Table.Header>

                    <Table.Body>
                    {this.props.data.length===0?
                        <Table.Row>
                            <Table.Cell>There is no data available</Table.Cell>
                        </Table.Row>
                    :
                        this.props.data
                        .filter((curData,i)=>{
                            const lim = 4*this.state.curActivePage
                            const checker = i+1
                            return checker===lim-3
                                ||checker===lim-2
                                ||checker===lim-1
                                ||checker===lim
                        })
                        .map((curData, i) =>{
                            noRows++;
                            return(
                                <Table.Row>
                                    {/* <Table.Cell>{curData.receipt_number}</Table.Cell> */}
                                    <Table.Cell>{new Date(curData.timestamp).toLocaleString()}</Table.Cell>
                                    <Table.Cell>{curData.number_of_items_purchased}</Table.Cell>
                                    <Table.Cell>{curData.age}</Table.Cell>
                                    {curData.gender===1?
                                        <Table.Cell>Male</Table.Cell>
                                    :
                                        <Table.Cell>Female</Table.Cell>
                                    }
                                    <Table.Cell>{curData.store_location}</Table.Cell>
                                    <Table.Cell>${Number(curData.total_receipt_amount).toFixed(2)}</Table.Cell>
                                </Table.Row>
                            )
                        })}
                    </Table.Body>

                    <Table.Footer>
                    <Table.Row>
                        <Table.HeaderCell colSpan='7'>
                        <Pagination 
                            defaultActivePage={1} 
                            totalPages={this.props.data.length/4>Math.floor(this.props.data.length/4)?
                                Math.floor(this.props.data.length/4)+1
                                :
                                Math.floor(this.props.data.length/4)} 
                            siblingRange={0}
                            lastItem={null}
                            firstItem={null}
                            onPageChange={this.handlePageChange}
                        />
                        </Table.HeaderCell>
                    </Table.Row>
                    </Table.Footer>
                </Table>

            </div>
        )
    }
}

Transactions.propTypes = {
    //Required Props
    data: PropTypes.array,

    //Optional Props
    
    //type: PropTypes.string,
}

export default Transactions