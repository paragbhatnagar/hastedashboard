import React, { Component } from 'react';
import { Container, Input, Button } from 'semantic-ui-react';

import logo from '../assets/img/haste-logo.png';
import '../assets/css/login.css';
import '../assets/css/theme.css';

// import styling and assets
class PasswordReset extends Component {
  render() {
    return (
      <div className="App">
        <Container fluid>
          <div className="loginContainer">
            <div className="loginCard">
              <img className="logo" src={logo} alt=""/>
              <h1>Password Reset</h1>
              <div className="loginInput">
                <Input fluid icon='mail' iconPosition='left' placeholder='Email Address' />
                <Button className="button">Reset Password</Button>
              </div>
            </div>
          </div>
        </Container>
      </div>
    );
  }
}

export default PasswordReset;
