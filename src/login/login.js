import React, { Component } from 'react';
import { Container, Input, Button } from 'semantic-ui-react';
import axios from 'axios';

import logo from '../assets/img/haste-logo.png';
import '../assets/css/login.css';
import '../assets/css/theme.css';
import {NavLink, Redirect} from "react-router-dom";

import {
  API_SERVER_URL,
  LOGIN,
} from '../api';

// import styling and assets
class Login extends Component {
  constructor(props){
    super(props);
    this.state = {
      email: null,
      merchant: null,
      password: null,
      loggedIn: false,
      errorMessage: null,
    }
  }

  async componentWillMount() {
    var session = localStorage.getItem("session");
    if(session !== null) {
      this.setState({loggedIn: true});
    }
  }

  async callPostAPI(api, body){
    let result = {};
    
    await fetch(API_SERVER_URL + api,{
      method: 'POST', 
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    })
    .then((response) => response.json())
    .then((responseJson)=>{
      result = responseJson;
    })
    return result;
  }
  
  login = async () => {
    // console.log(API_SERVER_URL+LOGIN);
    let result = await this.callPostAPI(LOGIN, {
      email: this.state.email,
      password: this.state.password,
    })
    if(!result.error){
      // console.log(result);
      localStorage.setItem("session", result.session)
      localStorage.setItem("merchant", result.merchant_name)
      console.log("merchant:", localStorage.getItem("merchant"))
      window.location.href = '/dashboard';
    }
    this.setState({
      errorMessage: result.error
    })
  }

  render() {
    if(this.state.loggedIn) {
      return <Redirect to='/dashboard'/>
    }

    return (
      <div className="App">
        <Container fluid>
          <div className="loginContainer">
            <div className="loginCard">
              <img className="logo" src={logo} alt=""/>
              <h1>Haste Login</h1>
              <div className="loginInput">
                <Input className="input-login" fluid icon='mail' iconPosition='left' placeholder='Email Address' onChange={(e, data) => this.setState({email: data.value})}/>
                <Input className="input-login" fluid icon='lock' type='password' iconPosition='left' placeholder='Password' onChange={(e, data) => this.setState({password: data.value})}/>
                {this.state.errorMessage}
                <Button className="button" onClick={this.login}>Login</Button>
                <NavLink to="/dashboard" className="sidebar-list-item-link">
                </NavLink>
              </div>
              <a href="">Forgot Password</a>
            </div>
          </div>
        </Container>
      </div>
    );
  }
}

export default Login;
