import React, { Component } from 'react';

import { Dropdown, Dimmer, Button , Loader} from 'semantic-ui-react';

//import components 
import SidebarMenu from '../components/sidebar';
import Navbar from '../components/navbar';

// import styling and assets
import '../assets/css/shopper.css';
import '../assets/css/App.css';
import Transactions from '../dashboard/components/dashboardTransactions';
import Elasticity from './components/elasticity';
import Affinity from './components/affinity';

// import api
import {
    API_SERVER_URL, 
    AVAILABLE_STORES,
} from '../api';

class Product extends Component {
    constructor(props){
        super(props);
        this.state = {
            shopperDisplay: 'affinity',
            durationOptions:[],
            selectedStore: null,
            storeOptions: [],
            is_loading: false
        }
    }

    async componentDidMount(){
        this.setState({is_loading:true});
        const refreshingData = this.refreshData();
        const gettingStores = this.getStores();

        await refreshingData;
        await gettingStores;
        this.setState({is_loading:false});
    }
    
    async callGetAPI(api){
        let result = {};
        await fetch(API_SERVER_URL + api,{
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
        .then((response) => { 
            console.log("GET response: ", response)
            response.json()
        })
        .then((responseJson)=>{
            result = responseJson;
        })

        return result;
    }

    async callPostAPI(api, body){
        let result = {};
        
        await fetch(API_SERVER_URL + api,{
            method: 'POST',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
        .then((response) => response.json())
        .then((responseJson)=>{
            result = responseJson;
        })

        return result;
    }

    async refreshData() {
    }

    async getStores(){
        // let s = await this.callPostAPI(AVAILABLE_STORES, {"merchant_name": "greenmart_development_database"})

        let s = [
            {
                "outlet_code": "H-GM-001",
                "outlet_name": " Development 1"  //Melon Supermarket
            },
            {
                "outlet_code": "H-GM-002",
                "outlet_name": "Development 2" // Yee Hong Seng
            }   
        ]

        let stores = s.map((store) => {
            console.log("allstore: ", s);
            return {
                value: store.outlet_code,
                text: store.outlet_name,
            }
        }); 
        this.setState({ 
            storeOptions: stores
        });

        console.log("Store Options: ", this.state.storeOptions);

    }

    render() {
        return (
            <div style={{display: 'flex'}}>
            <Dimmer active={this.state.is_loading}>
                <Loader> Cool things are coming your way... </Loader>
            </Dimmer>
                <SidebarMenu page="products"/>
                <div className="sales-container">
                    <Navbar 
                        refresh={this.refreshData}
                    />
                    <div className="full-width">
                        <div className="input-container full">
                            <p className="label">Store</p>
                            <Dropdown className="dropdown-selector" placeholder='Search for stores' 
                                options={this.state.storeOptions} 
                                search selection 
                                onChange={async (event,data) => {
                                    await this.setState({ selectedStore: data.value })
                                    console.log("Store value: ", this.state.selectedStore)
                                }}
                            />
                        </div>
                    </div>
                    <div className="sales-buttons">
                        <Button 
                            fluid
                            className={ "button " + (this.state.shopperDisplay === "affinity" ? "active" : "" )}
                            onClick={()=> this.setState({shopperDisplay: "affinity"})}>
                            Affinity
                        </Button>
                        <Button 
                            fluid
                            className={ "button " + (this.state.shopperDisplay === "elasticity" ? "active" : "" )}
                            onClick={()=> this.setState({shopperDisplay: "elasticity"})}>
                            Elasticity
                        </Button>
                    </div>
                    {this.state.shopperDisplay === "affinity"? (
                        <Affinity selectedStore={this.state.selectedStore}/>
                    ) : (
                        <Elasticity selectedStore={this.state.selectedStore}/>
                    )}
                </div>
            </div>
            
        );
    }
}

export default Product;
