import React, { Component } from 'react';
import { Dropdown, Button , Grid, Segment, Label , Divider, Icon} from 'semantic-ui-react';
// import DatePicker from 'react-datepicker';
import { Slider } from 'react-semantic-ui-range';
// import moment from 'moment';
import 'moment/locale/en-gb';
import pointer from '../../assets/img/pointer.png';

import "react-select/dist/react-select.css";
import "react-virtualized/styles.css";
import "react-virtualized-select/styles.css";

import Select from "react-virtualized-select";

import {
    API_SERVER_URL, 
    AVAILABLE_STORES,
    AFFINITYITEMTOCATEGORYSIMPLE,
    AFFINITITEMTOCATEGORYSIMPLE,
    AFFINITYSIMPLE,
    CATEGORIES,
    SUBCATEGORIES,
    PRODUCTS
} from '../../api';

// import styling and assets
import '../../assets/css/sales.css';
import '../../assets/css/products.css';

class Affinity extends Component {
    constructor(props){
        super(props);
        this.state = {
            //merchant_name: localStorage.getItem("merchant"),
            merchant_name: "greenmart_development_database",
            //category: [],
            categoryOptions: [],
            selectedCategory: null,

            categoryActive: null,
            
            subcategoryOptions: [],
            selectedSubcategory: [],

            //subcategory: [],
            subcategoryActive: null,
            
            productOptions: [],
            selectedProduct: null,

            //product: [],
            productActive: null,

            value: 0,
            affinitylist: [],
            errorAffinity: '',
            successaffinity: false,
            affinityresultlabel: '',
            affinityresultvalue: '',
            affinity_one: '',
            affinity_two: '',
            same_basket: '',
            one_all_basket: '',
            two_all_basket: '',
            one_label: '',
            two_label: '',
            viewMoreDetails: false
        };
    }

    async componentDidMount() {
        const body = {
            merchant_name: this.state.merchant_name
        }
        console.log(body)
        await this.getAllProducts(body);
        await this.getAllCategories(body);
        //await this.getAllSubCategories(body);
        //this.matchCategoryNameToID(body);

        //this.setState({ categoryOptions: this.state.category });   
    }

    handleValueChange(e, {value}){
        this.setState({
          value: value
        })
    }

    async callGetAPI(api){
        let result = {};
        await fetch(API_SERVER_URL + api,{
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
        .then((response) => response.json())
        .then((responseJson)=>{
            result = responseJson;
        })

        return result;
    }

    async callPostAPI(api, body){
        let result = {};
        
        await fetch(API_SERVER_URL + api,{
            method: 'POST', 
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
        .then((response) => response.json())
        .then((responseJson)=>{
            result = responseJson;
        })

        return result;
    }

    async getAllCategories(body) {
        let catResults = await this.callPostAPI(CATEGORIES, body);
        let categories = catResults.map((cat) => {
            return {
                value: cat.product_category_name,
                text: cat.product_category_name,
            }
        });

        // categories.unshift({
        //     value: 0,
        //     text: ''
        // })

        this.setState({ categoryOptions: categories })
    }

    // async getAllSubCategories(body) {
    //     let subcatResults = await this.callPostAPI(SUBCATEGORIES, body);
    //     if(subcatResults.length > 0 ) {
    //         let subcategories = subcatResults.map((cat) => {
    //             return {
    //                 value: cat.product_subcategory_id,
    //                 text: cat.product_subcategory_name,
    //                 category: cat.product_category_id
    //             }
    //         });
    
    //         this.setState({ subcategory: subcategories })
    //     }
    // }

    async getAllProducts(body) {
        let productResults = await this.callPostAPI(PRODUCTS, body);
        if(productResults.length > 0){
            let products = productResults.map((prod) => {
                return {
                    label: prod.product_name,
                    text: prod.product_name,
                    value: prod.product_merchant_sku,
                    category: prod.product_category_name
                }
            });

            // products.unshift({
            //     value: 0,
            //     text: '',
            //     label: ''
            // })
                
            this.setState({ productOptions: products })
        }
    }

    // matchCategoryNameToID() {
    //     let products = this.state.product.map((prod) => {
    //         let product_category_name = prod.category
    //         let product_category_id = this.state.category.filter(cat => cat.name === product_category_name);
    //         return {
    //             value: prod.value,
    //             text: prod.text,
    //             label: prod.label,
    //             category: product_category_id,
    //             categoryName: prod.category,
    //         }
    //     });

    //     this.setState({ product: products });
    // }

    async setActiveCategory(selected) {
        // await this.setState({ categoryActive: selected });
        // if ( this.state.categoryActive === 0 ) {
        //     this.setState({ 
        //         subcategoryOptions: this.state.subcategory,
        //         productOptions: this.state.product,
        //         subcategoryActive: 1,
        //         productActive: 1,
        //     });
        // } else {
        //     let subcategories = this.state.subcategory.filter(sc => sc.category === this.state.categoryActive);
            
        //     await this.setState({
        //         subcategoryOptions: subcategories,
        //         productOptions: this.state.product,
        //         subcategoryActive: 1,
        //         productActive: 1,
        //     });

        //     for (var i = 0; i < this.state.categoryOptions.length; i++) {
        //         if(this.state.categoryOptions[i].value == selected){
        //             this.addToAffinityList(this.state.categoryOptions[i], 'category');
        //         }
        //     }
        // }
        for (var i = 0; i < this.state.categoryOptions.length; i++) {
            if(this.state.categoryOptions[i].value == selected){
                this.addToAffinityList(this.state.categoryOptions[i], 'category');
            }
        }
    }

    // async setActiveSubcategory(selected) {
    //     await this.setState({ subcategoryActive: selected });
        
    //     if ( this.state.subcategoryActive === 0 ) {
    //         this.setState({ 
    //             productOptions: this.state.product,
    //             productActive: 1,
    //         });
    //     } else {
    //         let products = this.state.product.filter(prod => prod.category == this.state.categoryActive);
    //         this.setState({
    //             productOptions: products,
    //             productActive: 1,
    //         });
    //     }
    // }

    async setActiveProduct(selected) {
        // if (selected.value != 0) {
        //     //await this.setState({ selectedProduct: selected.value });
        //     console.log(this.state.selectedProduct)

        //     for (var i = 0; i < this.state.productOptions.length; i++) {
        //         if(this.state.productOptions[i].value == selected.value){
        //             this.addToAffinityList(this.state.productOptions[i], 'product');
        //         }
        //     }
        // }else{
        //     this.setState({ selectedProduct: '' });
        // }
        await this.setState({ selectedProduct: selected.value });
        console.log(this.state.selectedProduct)

        for (var i = 0; i < this.state.productOptions.length; i++) {
            if(this.state.productOptions[i].value == selected.value){
                this.addToAffinityList(this.state.productOptions[i], 'product');
            }
        }

    }

    affinitylist(){
        var rows = [];
        this.state.affinitylist.forEach(function(e) {
            rows.push(
                <div className="input-container2" key={1}>
                    <Segment>
                        <p>
                            <a className="btn"
                                onClick= {
                                ()=> this.removeFromAffinityList(e.label)
                                }>
                                <i className="close icon"></i>
                            </a>
                            {e.label}
                        </p> 
                    </Segment>
                </div>
            );
        }, this);

        return <div className="analysis-options">{rows}</div>;
    }

    removeFromAffinityList(label){
        if(this.state.affinitylist.length > 1){
            var data = this.state.affinitylist;

            data.forEach(function(e) {
                if(label != e.label){
                    this.setState({
                        affinitylist:[e]
                    })
                }
            }, this);
        }else{
            this.setState({
                affinitylist:[]
            })
        } 
    }

    addToAffinityList(data, type){
        var affinityList = this.state.affinitylist;
        if(affinityList.length < 2){
            if(this.state.affinitylist[0]){
                if(this.state.affinitylist[0].label == data.text){
                    return;
                }
            }

            affinityList.push({label: data.text, data: data, type: type });
            this.setState({
                affinitylist: affinityList
            })
        }else{
            this.setState({
                errorAffinity: 'Only 2 Item can be added, Please Add 2 Products / 1 Product & 1 Category / 2 Categories to generate affinity'
            })
        }
    }

    async generateaffinity(){
        if(this.state.affinitylist.length == 2){
            if(!this.props.selectedStore){
                alert('Please select outlet');
                return;
            }
            let productCount = 0;
            let categoryCount = 0;
            let product = [];
            let category = [];

            for (let index = 0; index < this.state.affinitylist.length; index++) {
                if(this.state.affinitylist[index].type == 'category'){
                    categoryCount += 1;
                    category.push(this.state.affinitylist[index]);
                }else if(this.state.affinitylist[index].type == 'product'){
                    productCount += 1;
                    product.push(this.state.affinitylist[index]);
                }
            }

            if(productCount ==2 && categoryCount == 0){
                //2 product affinity
                let a = await this.callPostAPI(AFFINITYSIMPLE, { 
                    "product_1_sku": product[0].data.value,
                    "product_2_sku": product[1].data.value,
                    "outlet_code" : this.props.selectedStore,
                    "start_datetime": "2018-01-01",
                    "end_datetime": "2018-05-31"
                  })
                
                if(a.result != {}){
                    if(a.result.hasOwnProperty('affinity')){
                        this.setState({errorAffinity: ''})
                        this.setState({successaffinity: true})
                        this.setState({
                            affinityresultlabel: "Affinity for '"+product[0].label+"' and '"+product[1].label+ "'",
                            affinityresultvalue: a.result.affinity + '%',
                            affinity_one: a.result.affinity_x_based + '%',
                            affinity_two: a.result.affinity_y_based + '%',
                            same_basket: a.result.xy_same_basket,
                            one_all_basket: a.result.x_all_basket,
                            two_all_basket: a.result.y_all_basket,
                            one_label: product[0].label,
                            two_label: product[1].label
                        })
                    }else{
                        this.setState({
                            errorAffinity: "No Transaction for '"+product[0].label+"' and '"+product[1].label+ "'. Please try with another product",
                            successaffinity: false
                        })
                    }
                }else{
                    this.setState({
                        errorAffinity: 'Empty result. Please try with another product',
                        successaffinity: false
                    })
                }

            }else if(productCount == 1 && categoryCount == 1){
                //1 product 1 category
                let a = await this.callPostAPI(AFFINITITEMTOCATEGORYSIMPLE, { 
                    "product_sku": product[0].data.value,
                    "category": category[0].data.value,
                    "outlet_code" : this.props.selectedStore,
                    "start_datetime": "2018-01-01",
                    "end_datetime": "2018-05-31"
                  })
                
                if(a.result != {}){
                    if(a.result.hasOwnProperty('affinity')){
                        this.setState({errorAffinity: ''})
                        this.setState({successaffinity: true})
                        this.setState({
                            affinityresultlabel: "Affinity for '"+product[0].label+"' and '"+category[0].label+ "'",
                            affinityresultvalue: a.result.affinity + '%',
                            affinity_one: a.result.affinity_x_based + '%',
                            affinity_two: a.result.affinity_c_based + '%',
                            same_basket: a.result.xc_same_basket,
                            one_all_basket: a.result.x_all_basket,
                            two_all_basket: a.result.c_all_basket,
                            one_label: product[0].label,
                            two_label: category[0].label
                        })
                    }else{
                        this.setState({
                            errorAffinity: "No Transaction for '"+product[0].label+"' and '"+category[0].label+ "'. Please try with another product / category",
                            successaffinity: false
                        })
                    }
                    
                }else{
                    this.setState({
                        errorAffinity: 'Empty result. Please try with another product / category',
                        successaffinity: false
                    })
                }
            }else if(productCount == 0 && categoryCount == 2){
                //2 category
                let a = await this.callPostAPI(AFFINITYITEMTOCATEGORYSIMPLE, { 
                    "category_1": category[0].data.value,
                    "category_2": category[1].data.value,
                    "outlet_code" : this.props.selectedStore,
                    "start_datetime": "2018-01-01",
                    "end_datetime": "2018-05-31"
                  })
                  
                if(a.result != {}){
                    
                    if(a.result.hasOwnProperty('affinity')){
                        this.setState({errorAffinity: ''})
                        this.setState({successaffinity: true})
                        this.setState({
                            affinityresultlabel: "Affinity for '"+category[0].label+"' and '"+category[1].label+ "'",
                            affinityresultvalue: a.result.affinity + '%',
                            affinity_one: a.result.affinity_c2_based + '%',
                            affinity_two: a.result.affinity_c1_based + '%',
                            same_basket: a.result.c1c2_same_basket,
                            one_all_basket: a.result.c1_all_basket,
                            two_all_basket: a.result.c2_all_basket,
                            one_label: category[0].label,
                            two_label: category[1].label
                        })
                    }else{
                        
                        this.setState({
                            errorAffinity: "Insufficient transactions data for '"+category[0].label+"' and '"+category[1].label+ "'. Please try with another category",
                            successaffinity: false
                        })
                        
                    }
                }else{
                    this.setState({
                        errorAffinity: 'Empty result. Please try with another category',
                        successaffinity: false
                    })
                }
                
            }else{
                this.setState({successaffinity: false})
                this.setState({errorAffinity: 'Please Add 2 Product / 1 Product & 1 Category / 2 Category to generate affinity'})
            }

        }else{
            this.setState({successaffinity: false})
            this.setState({errorAffinity: 'Please Add 2 Product / 1 Product & 1 Category / 2 Category to generate affinity'})
        }
    }
    
    render() {
        // const settings = {
        //     start:2,
        //     min:0,
        //     max:10,
        //     step:1,
        // }

        return (
            <div className="analysis">
                <div className="analysis-options">
                    <h2>Combine and test affinity</h2>
                </div>
                <div className="analysis-options">

                    <div className="input-container">
                        <p className="label">Category</p>
                        <Dropdown className="dropdown-selector" placeholder="Select Category"
                            selection value={this.state.selectedCategory}
                            search selection
                            closeOnChange = {true}
                            options={this.state.categoryOptions} 
                            onChange={
                                async (event, data) => { 
                                    await this.setState({ 
                                        selectedCategory: data.value 
                                    })
                                    this.setActiveCategory(data.value) 
                                }
                            }
                        />
                    </div>

                    {/* <div className="input-container">
                        <p className="label">Sub-category</p>
                        <Dropdown className="dropdown-selector" placeholder="Category"
                            selection value={this.state.categoryActive}
                            options={this.state.categoryOptions} 
                            // onChange={async (event, data) => { this.setActiveSubcategory(data.value) }}
                        />
                    </div> */}

                    <div className="input-container">
                        <p className="label">Product</p>
                        <Select 
                            closeOnSelect={true}
                            clearable={true}
                            disabled={false}
                            //multi={true}
                            options={this.state.productOptions} 
                            value={this.state.selectedProduct} 
                            closeOnSelect={true}
                            //onChange={ async (data) => {
                            //    this.setActiveProduct(data)
                            //}} 
                            onChange={ (selected)=>{
                                console.log(selected)
                                    this.setState({selectedProduct: selected})
                                    this.addToAffinityList(selected, 'product');
                                    //this.setActiveProduct(selected)
                                }
                            }
                            searchable={true}
                            labelKey='label'
                            valueKey='label'
                        />
                    </div>
                </div>

                <div>
                    <div>
                        <Grid padded>
                            <Grid.Column width={16}>
                                <Segment className="affinity-card">
                                    {/* <h2>Choose shopper profiles</h2>
                                    <div className="shopper-checkbox-list">
                                        <label className="checkboxcontainer">
                                            <input type="checkbox"/>
                                            <span className="checkmark"></span>
                                            All Shopper(s)
                                        </label>
                                        <label className="checkboxcontainer">
                                            <input type="checkbox"/>
                                            <span className="checkmark"></span>
                                            Shopper A
                                        </label>

                                        <label className="checkboxcontainer">
                                            <input type="checkbox"/>
                                            <span className="checkmark"></span>
                                            Shopper B
                                        </label>

                                        <label className="checkboxcontainer">
                                            <input type="checkbox"/>
                                            <span className="checkmark"></span>
                                            Shopper C
                                        </label>

                                        <label className="checkboxcontainer">
                                            <input type="checkbox"/>
                                            <span className="checkmark"></span>
                                            Shopper D
                                        </label>
                                    </div> */}

                                    <Segment className="affinity-card" style={{width: '70%', margin: '5%'}}>
                                        {
                                            this.affinitylist()
                                        }

                                        {
                                            this.state.affinitylist.length < 1?
                                            <div style={{display: 'flex', flexDirection: 'column', alignItems: 'center'}}>
                                                <img src={pointer} alt="" style={{maxWidth:500}} />
                                                <h3>ADD CATEGORY, SUBCATEGORY OR PRODUCT</h3>
                                                <p style={{fontSize: 17}}>(2 Products / 1 Product & 1 Category / 2 Categories)</p>
                                            </div>
                                            :null
                                        }
                                    </Segment>
                                    
                                    <Button
                                        className="button"
                                        id="data-button" style={{width: 200, backgroundColor: '#FB8361', color: 'white' }} 
                                        onClick={ ()=>
                                            {this.generateaffinity()}
                                        }
                                        >
                                        GENERATE AFFINITY
                                    </Button>
                                    <div><p style={{color: 'red'}}> {this.state.errorAffinity} </p></div>

                                        {
                                            this.state.successaffinity?
                                            <div style={{width: '70%'}}>
                                                    <div className="analysis-options">
                                                        <h2>{this.state.affinityresultlabel}</h2>
                                                    </div>
                                                    <div className="analysis-options">
                                                        <div className="input-container">
                                                            <Segment className="affinity-card" style={{textAlign: 'center', width: '100%'}}>
                                                                <p>Affinity Based on {this.state.one_label}</p>
                                                                <h1 style={{color: '#FB8361', marginTop: 0} }>{this.state.affinity_one}</h1>
                                                            </Segment>
                                                        </div>

                                                        <div className="input-container">
                                                            <Segment className="affinity-card" style={{textAlign: 'center', width: '100%'}}>
                                                                <p>General Affinity</p>
                                                                <h1 style={{color: '#FB8361', marginTop: 0} }>{this.state.affinityresultvalue}</h1>
                                                            </Segment>
                                                        </div>
                                                        <div className="input-container">
                                                            <Segment className="affinity-card" style={{textAlign: 'center', width: '100%'}}>
                                                                <p>Affinity Based on {this.state.two_label}</p>
                                                                <h1 style={{color: '#FB8361', marginTop: 0} }>{this.state.affinity_two}</h1>
                                                            </Segment>
                                                        </div>

                                                    </div>

                                                    <div className="analysis-options">
                                                        <div className="input-container">
                                                            <Segment className="affinity-card" style={{textAlign: 'center', width: '100%'}}
                                                                onClick={ ()=>
                                                                    this.setState({viewMoreDetails: !this.state.viewMoreDetails})
                                                                }
                                                            >
                                                                <p>View More Details: 
                                                                    {this.state.viewMoreDetails?
                                                                        <Icon size="large" name="angle up"/>
                                                                        :
                                                                        <Icon size="large" name="angle down"/>
                                                                    }
                                                                </p>
                                                                {
                                                                    this.state.viewMoreDetails?
                                                                    <div>
                                                                        <p>Same Basket :</p><h4 style={{color: '#FB8361', padding: 0, marginBottom: 3} }>{this.state.same_basket}</h4>
                                                                        <p>{this.state.one_label} Basket:</p><h4 style={{color: '#FB8361', padding: 0, marginBottom: 3} }>{this.state.one_all_basket}</h4>
                                                                        <p>{this.state.two_label} Basket:</p><h4 style={{color: '#FB8361', padding: 0, marginBottom: 3} }>{this.state.two_all_basket}</h4>
                                                                    </div>
                                                                    :
                                                                    null
                                                                }
                                                            </Segment>
                                                        </div>
                                                    </div>
                                                    
                                                </div>
                                            :null
                                        }
                                </Segment>
                            </Grid.Column>
                        </Grid>
                    </div>
                </div>
            </div>
        );
    }
}

export default Affinity;
