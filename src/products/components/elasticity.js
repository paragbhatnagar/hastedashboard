import React, { Component } from 'react';
import { Dropdown, Button , Grid, Segment, Label, Input } from 'semantic-ui-react';
// import DatePicker from 'react-datepicker';
import { Slider } from 'react-semantic-ui-range';
// import moment from 'moment';
import 'moment/locale/en-gb';

import "react-select/dist/react-select.css";
import "react-virtualized/styles.css";
import "react-virtualized-select/styles.css";

import Select from "react-virtualized-select";

import {
    API_SERVER_URL, 
    AVAILABLE_STORES,
    // SALES_SUMMARY,
    // ANALYSIS_SALES_TIME_GRAPH_DATA,
    // ANALYSIS_AVG_TICKET_GRAPH_DATA,
    // ANALYSIS_AVG_BASKET_GRAPH_DATA,
    CATEGORIES,
    SUBCATEGORIES,
    PRODUCTS,
    PRICEELASTICITYOFDEMAND_PROMO
} from '../../api';

//import components 
// import Chart from '../../components/chart';


// import styling and assets
import '../../assets/styles/sales.scss';

class Analysis extends Component {
    constructor(props){
        super(props);
        this.state = {
            //PLACEHOLDER VALUES. TO BE REPLACED WITH DEFAULT VALUES
            //Selections data
            category: [],
            categoryActive: null,
            categoryActiveName: null,
            categoryOptions: [],
            subcategory: [],
            subcategoryActive: null,
            subcategoryOptions: [],
            product: [],
            productActive: null,
            productOptions: [],
            selectedStore: null,
            storeOptions:[],
            sliderValue1: 0.01,
            sliderMax: 1,
            value: 0,
            current_selling_price: 0,
            current_profit: 0,
            current_margin: 0,
            linear_slope: 0,
            coefficient_b: 0,
            cost_price: 0,
            new_selling_price: 0,
            new_profit_for_sale : 0,
            quantity_in_normal: 0,
            new_quantity: 0,
            slideValueColor: 'grey',
        };
        
    }

    async callGetAPI(api){
        let result = {};
        await fetch(API_SERVER_URL + api,{
            method: 'GET',
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
        })
        .then((response) => response.json())
        .then((responseJson)=>{
            result = responseJson;
        })

        return result;
    }

    async callPostAPI(api, body){
        let result = {};
        
        await fetch(API_SERVER_URL + api,{
            method: 'POST', 
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(body)
        })
        .then((response) => response.json())
        .then((responseJson)=>{
            result = responseJson;
        })

        return result;
    }
    
    // ##############################
    // // // Get Selection Data Methods
    // ##############################
    async getCategoryData() {
        let c = await this.callPostAPI(CATEGORIES, {"merchant_name": "greenmart_development_database"});
        // Set Category Options
        let categories = c.map((cat) => {
            return {
                value: cat.product_category_name,
                // value: cat.product_category_id,
                text: cat.product_category_name,
            }
        });

        categories.unshift({
            value: 0,
            text: 'ALL CATEGORIES',
        });

        this.setState({ category: categories })
    }

    async getSubcategoryData() {
        let sc = await this.callPostAPI(SUBCATEGORIES, {"merchant_name": "greenmart_development_database"});
        
        // Set Category Options
        if(sc.length > 0 ) {
            let subcategories = sc.map((cat) => {
                return {
                    value: cat.product_subcategory_id,
                    text: cat.product_subcategory_name,
                    category: cat.product_category_id
                }
            });
    
            subcategories.unshift({
                value: 0,
                text: 'ALL SUBCATEGORIES',
            });
    
            this.setState({ subcategory: subcategories })
        
        }
    }

    async getProductData(term) {
        
        let p = await this.callPostAPI(PRODUCTS, {"merchant_name": "greenmart_development_database"});
        // Set Category Options
        
        if(p.length > 0){
            let products = p.map((prod) => {
                return {
                    label: prod.product_name,
                    text: prod.product_name,
                    value: prod.product_merchant_sku,
                    category: prod.product_category_name
                }
            });
            
            products.unshift({
                label: 'ALL PRODUCTS',
                value: 0,
            });
    
            this.setState({ product: products })
            
        }
        
    }

    matchCategoryNameToID() {
        let products = this.state.product.map((prod) => {
            let product_category_name = prod.category
            let product_category_id = this.state.category.filter(cat => cat.name === product_category_name);
            return {
                value: prod.value,
                text: prod.text,
                label: prod.label,
                category: product_category_id,
                categoryName: prod.category,
            }
        });

        this.setState({ product: products });
    }

    async getStores() {
        let s = await this.callPostAPI(AVAILABLE_STORES, {"merchant_name": "greenmart_development_database"})
        
        let stores = s.map((store) => {
            return {
                value: store.outlet_id,
                text: store.outlet_name,
            }
        }); 
        this.setState({ 
            storeOptions: stores
        });

    }

    async componentDidMount() {
        await this.getCategoryData();
        await this.getSubcategoryData();
        await this.getProductData();

        this.matchCategoryNameToID();

        this.setState({ categoryOptions: this.state.category });
        
    }

    async setActiveCategory(value) {
        await this.setState({ categoryActive: value });

        if ( this.state.categoryActive === 0 ) {
            this.setState({ 
                subcategoryOptions: this.state.subcategory,
                productOptions: this.state.product,
                subcategoryActive: 1,
                productActive: 1,
            });
        } else {
            let subcategories = this.state.subcategory.filter(sc => sc.product_category_id == this.state.categoryActive);

            let products = this.state.product.filter(prod => prod.categoryName == this.state.categoryActive);
            await this.setState({
                subcategoryOptions: subcategories,
                productOptions: products,
                subcategoryActive: 1,
                productActive: 1,
            });
        }
    }

    async setActiveSubcategory(value) {
        await this.setState({ subcategoryActive: value });
        
        if ( this.state.subcategoryActive === 0 ) {
            this.setState({ 
                productOptions: this.state.product,
                productActive: 1,
            });
        } else {
            let products = this.state.product.filter(prod => prod.category == this.state.categoryActive);
            this.setState({
                productOptions: products,
                productActive: 1,
            });
        }
    }

    setActiveProduct(value) {
        
        if (value) {
            this.setState({ productActive: value.value });
        }else{
            this.setState({ productActive: '' });
        }
    }

    async getGraphData(){
        var productActive = this.state.productActive;
        if(!productActive){
            console.log('Please select product');
        }else if(!this.props.selectedStore){
            alert('Please select outlet');
        }else{
            let g = await this.callPostAPI(PRICEELASTICITYOFDEMAND_PROMO, {
                "product_merchant_sku": productActive, 
                "outlet_code": this.props.selectedStore,
                "start_datetime": "2018-01-01",
                "end_datetime": "2018-05-31"
            });
            
            if(g.result.has_promo){
                if(g.result.promotions){
                    if(g.result.promotions[0]){
                        g = g.result.promotions[0];
                        var profits = (Math.floor(g.quantity_in_normal) * g.normal_price) - (Math.floor(g.quantity_in_normal) * g.cost_price);
                        var margin = profits / (Math.floor(g.quantity_in_normal)* g.cost_price);

                        this.setState({
                            current_selling_price: g.normal_price,
                            current_profit: profits,
                            current_margin: margin,
                            linear_slope: g.linear_slope,
                            coefficient_b: g.coefficient_b,
                            cost_price: g.cost_price,
                            quantity_in_normal: g.quantity_in_normal,
                            sliderMax: g.price_when_zero_demand,
                            new_quantity: g.price_when_zero_demand,
                        })
                        
                        const e = new Event('input', { bubbles: true })
                        this.handleValueChange(e, g.normal_price)
                    }
                }
            }else{
                alert('This product has insufficient data to calculate price elasticity. Please try another product');
            }
        }
    }

    sliderValueChange(value){
        
        this.setState({
            sliderValue1 : value
        })

        var q = this.state.linear_slope * value + this.state.coefficient_b;
        var newProfit = (Math.floor(q)*value) - (Math.floor(q) * this.state.cost_price);

        this.setState({
            new_selling_price: value,
            new_quantity: q,
            new_profit_for_sale : q * (value - this.state.cost_price)
        })

        if(q < (this.state.quantity_in_normal*90/100)){
            this.state.slideValueColor = 'red';
        }else{
            this.state.slideValueColor = 'grey';
        }
        
    }

    handleValueChange(e, value){
        console.log(e)
        this.setState({
            sliderValue1: value
        })
        this.sliderValueChange(value);
    }
    
    theslider() {
        console.log(this.state.sliderValue1);
        const settings = {
            start: this.state.sliderValue1,
            min:0.01,
            max:this.state.sliderMax,
            step:0.01,
            onChange: (value) => this.sliderValueChange(value)
        }
        return(
            <div>
                <Slider value={this.state.sliderValue1} color="grey" inverted={false} settings={settings}/>
                {/* <Input placeholder="Enter Value" value={this.state.sliderValue1} onChange={ (e) => this.handleValueChange(e, 1)}/> */}
            </div>
        )
    }
    // ##############################
    // // // Renderers
    // ##############################
    
    render() {


        return (
            <div className="analysis">
                
                <div className="analysis-options">

                    <div className="input-container">
                        <p className="label">Category</p>
                        <Dropdown className="dropdown-selector" placeholder="Category"
                            selection value={this.state.categoryActive}
                            options={this.state.categoryOptions} 
                            onChange={async (event, data) => { this.setActiveCategory(data.value) }}
                        />
                    </div>

                    {/* <div className="input-container">
                        <p className="label">Sub-category</p>
                        <Dropdown className="dropdown-selector" placeholder="Category"
                            selection value={this.state.categoryActive}
                            options={this.state.categoryOptions} 
                            // onChange={async (event, data) => { this.setActiveSubcategory(data.value) }}
                        />
                    </div> */}

                    <div className="input-container">
                        <p className="label">Product</p>
                        {/* <Dropdown className="dropdown-selector" placeholder="Product" 
                            selection value={this.state.productActive}
                            options={this.state.productOptions} 
                            onChange={async (event, data) => { this.setActiveProduct(data.value) }}
                        /> */}
                        <Select 
                            options={this.state.productOptions} 
                            // className="dropdown-selector" 
                            value={this.state.productActive} 
                            // multi={false}
                            searchable={true}
                            closeOnSelect={true}
                            onChange={ async (data) => {this.setActiveProduct(data)}}
                        />
                    </div>

                    
                    <div className="input-container">    
                        <Button
                            className="button"
                            id="data-button"
                            onClick={() => this.getGraphData()}>
                            View Data
                        </Button>
                    </div>
                </div>
                <div>
                    {/* <div style={{marginLeft: '20px', marginRight: '20px'}}>
                        <p style={{marginBottom: '5px'}}>Test Price Elasticity for</p>
                        <Dropdown className="dropdown-selector" placeholder="Shopper Profile"
                            selection value={this.state.categoryActive}
                            options={this.state.categoryOptions} 
                            onChange={async (event, data) => {
                                await this.setState({ categoryActive: data.value })
                                console.log("Category Options: ", this.state.categoryActive)
                                this.getProductData()
                            }}
                        />
                    </div> */}
                    <div>
                    <Grid padded>
                        <Grid.Column width={16}>
                        <Segment className="slider-card">
                            <Label color={this.state.slideValueColor} className="slider-value">{this.state.slideValueColor == 'grey'? this.state.sliderValue1: "Too Expensive ( "+this.state.sliderValue1+" )"}</Label>
                            <div className="slider-container">
                                {this.theslider()}
                            </div>
                        </Segment>
                        </Grid.Column>
                    </Grid>
      
                    </div>
                    <div>
                        <div className="analysis-stats">
                            <div className="card" id="avg-ticket-size">
                                <h3 className="title">Current Selling Price</h3>
                                <h2 className="value">${Number(this.state.current_selling_price).toFixed(2)}</h2>
                            </div>
                            <div className="card" id="avg-basket-size">
                                <h3 className="title">Current Profits</h3>
                                <h2 className="value">${Number(this.state.current_profit).toFixed(2)}</h2>
                                
                            </div>
                            <div className="card" id="no-of-transactions">
                                <h3 className="title">Current Profit Margin</h3>
                                <h2 className="value">{Number(this.state.current_margin).toFixed(2)}%</h2>
                                
                            </div>
                        </div>
                        <div className="analysis-stats">
                            <div className="card" id="avg-ticket-size">
                                <h3 className="title">New Selling Price</h3>
                                <h2 className="value">${Number(this.state.new_selling_price).toFixed(2)}</h2>
                            </div>
                            <div className="card" id="avg-basket-size">
                                <h3 className="title">Expected Demand</h3>
                                <h2 className="value">{Number(this.state.new_quantity).toFixed(2)}</h2>
                            </div>
                            <div className="card" id="avg-basket-size">
                                <h3 className="title">Expected Profits</h3>
                                <h2 className="value">${Number(this.state.new_profit_for_sale).toFixed(2)}</h2>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        );
    }
}

export default Analysis;
