import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';

// import pages
import Login from './login/login';
import Dashboard from './dashboard/dashboard';
import Sales from './sales/sales';
import PasswordReset from './login/passwordReset';
import Products from './products/products';
import ShoppersAnalysis from './shoppers/shoppersAnalysis';
import Promotions from './promotions/promotions';

// import styling and assets
import './assets/css/App.css';

class App extends Component {

  render() {
    // Place all routes in this component
    return (
      <div>
        <Switch> 
          <Route exact path='/' component={Login}/> 
          <Route path='/dashboard/' component={Dashboard}/> 
          <Route path='/sales/' component={Sales}/> 
          <Route path='/password-reset/' component={PasswordReset}/>
          <Route path='/products/' component={Products}/>
          <Route path='/shoppers-analysis/' component={ShoppersAnalysis}/>
          <Route path='/promotions/' component={Promotions}/>
        </Switch>
      </div>
    );
  }
}

export default App;
